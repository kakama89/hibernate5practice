package hibernate5tut.practice.mapping.recursive;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class MainRecursive {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainRecursive.class);

    public static void main(String[] args) {
        MainRecursive.getAllMenu(DatabaseVendor.MYSQL);
    }

    public static void getAllMenu(DatabaseVendor vendor) {
        Function<Session, List<Menu>> func = session -> {
            Query<Menu> query = session.createQuery("SELECT distinct m From Menu m LEFT JOIN fetch m.children WHERE m.parent is null order by m.priority ASC ", Menu.class);
            return query.list();
        };
        List<Menu> listMenu = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> classes  : {}", listMenu);
    }
}
