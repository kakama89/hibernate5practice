package hibernate5tut.practice.mapping.onetoone;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "tbl_account_detail")
public class AccountDetails {
    @Id
    @Column(name = "account_id")
    private Long accountId;

    @MapsId // share primary key (u can setUser instead of setUserId)
    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "avatar_url")
    private String avatarUrl;

    @Column(name = "phone_number")
    private String phoneNumber;
}
