package hibernate5tut.practice.mapping.primary;

import hibernate5tut.practice.mapping.primary.UserResourceAction.Status;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Function;

/**
 * Mapping
 * {@link hibernate5tut.practice.fetch.lazy.Clazz#id}
 * {@link hibernate5tut.practice.fetch.lazy.Student#id}
 * {@link hibernate5tut.practice.fetch.eager.Company#id}
 * {@link hibernate5tut.practice.fetch.eager.Product#id}
 * {@link hibernate5tut.practice.state.Person#id}
 * {@link hibernate5tut.practice.mapping.recursive.Menu#id}
 * {@link hibernate5tut.practice.query.named.Client#id}
 * {@link hibernate5tut.practice.query.named.Site#id}
 * {@link hibernate5tut.practice.mapping.primary.UserResourceAction#id}
 * {@link hibernate5tut.practice.inheritance.singletable.AbstractEmployee#id}
 * {@link hibernate5tut.practice.inheritance.join.AbstractUser#id}
 */
public class MainPrimary {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainPrimary.class);

    public static void main(String... arg) {
        MainPrimary.createURA(DatabaseVendor.MYSQL, 1L, 2L, 3L, "1L_2L_3L");
        // MainPrimary.getURA(DatabaseVendor.MYSQL, 1L, 2L, 3L);

    }

    public static void createURA(DatabaseVendor vendor, Long userId, Long resourceId, Long actionId, String desc) {
        Function<Session, Void> func = session -> {
            UserResourceActionId id = new UserResourceActionId(userId, resourceId, actionId);
            UserResourceAction ura = new UserResourceAction(id);
            ura.setDescription(desc);
            ura.setStatus(Status.ACTIVE);
            session.persist(ura);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }


    public static void getURA(DatabaseVendor vendor, Long userId, Long resourceId, Long actionId) {
        Function<Session, UserResourceAction> func = session -> {
            UserResourceActionId id = new UserResourceActionId(userId, resourceId, actionId);
            return session.get(UserResourceAction.class, id);
        };
        UserResourceAction ura = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> Get URA {}", ura);
    }
}
