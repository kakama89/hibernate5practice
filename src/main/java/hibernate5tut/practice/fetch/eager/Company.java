package hibernate5tut.practice.fetch.eager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "tbl_company")
@NoArgsConstructor
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_company_id_seq_generator")
    @SequenceGenerator(name = "tbl_company_id_seq_generator", sequenceName = "tbl_company_id_seq", allocationSize = 1)
    private Long id;


    @Column(name = "name")
    private String name;

    /*
    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    @Fetch(FetchMode.JOIN)   // you can also use fetch mode JOIN to eager load even if fetch type is LAZY
    */
    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER)
    private Set<Product> products;

    @Override
    public String toString() {
        return MessageFormat.format("Company[id {0}, name {1}]", id, name);
    }
}
