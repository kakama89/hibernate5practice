package hibernate5tut.practice.inheritance.join;

import hibernate5tut.practice.inheritance.singletable.MainSingleTable;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Random;
import java.util.function.Function;

public class MainJoin {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainSingleTable.class);

    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;
        MainJoin.getAllUser(vendor);
//        MainJoin.createShipper(vendor, true);
//        MainJoin.createSender(vendor, "000", "TAX_000", "000@mail.com", 1L);
//        MainJoin.createReceiver(vendor, 180L, 34L);

    }

    public static void getAllUser(DatabaseVendor vendor) {
        Function<Session, List<AbstractUser>> func = session -> {
            List<AbstractUser> users = session.createQuery("From AbstractUser u", AbstractUser.class).list();
            return users;
        };
        List<AbstractUser> users = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>> Users : {}", users);
    }

    public static void createShipper(DatabaseVendor vendor, Boolean outputLimit) {
        Function<Session, Void> func = session -> {
            Shipper shipper = new Shipper();
            shipper.setName("Shipper " + new Random().nextInt(100));
            shipper.setOutputLimit(outputLimit);
            shipper.setStatus(UserStatus.ACTIVE);
            session.persist(shipper);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }


    public static void createSender(DatabaseVendor vendor, String cardId, String taxId, String email, Long shipperId) {
        Function<Session, Void> func = session -> {
            Sender sender = new Sender();
            sender.setName("Sender " + new Random().nextInt(100));
            sender.setCardId(cardId);
            sender.setTaxId(taxId);
            sender.setEmail(email);
            sender.setStatus(UserStatus.ACTIVE);
            sender.setShipper(session.get(Shipper.class, shipperId));
            session.persist(sender);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }


    public static void createReceiver(DatabaseVendor vendor, Long remoteAreaFee, Long provinceId) {
        Function<Session, Void> func = session -> {
            Receiver receiver = new Receiver();
            receiver.setName("Receiver " + new Random().nextInt(100));
            receiver.setProvince(provinceId);
            receiver.setRemoteAreaFee(remoteAreaFee);
            receiver.setStatus(UserStatus.ACTIVE);
            session.persist(receiver);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }


}
