package hibernate5tut.practice.query.criteria;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.MessageFormat;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {
    private Long id;
    private Long count;
    private Double resitsAvg;

    @Override
    public String toString() {
        return MessageFormat.format("\nStudentDTO[id {0}, count {1}, resitsAvg {2}]", id, count, resitsAvg);
    }
}
