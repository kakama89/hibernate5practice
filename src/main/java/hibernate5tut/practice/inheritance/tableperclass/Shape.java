package hibernate5tut.practice.inheritance.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "tbl_shape")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Shape {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "tbl_shape_id_seq_generator") // strategy = GenerationType.SEQUENCE postgres
    @SequenceGenerator(name = "tbl_shape_id_seq_generator", sequenceName = "tbl_shape_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "shape_name")
    private String shapeName;
}
