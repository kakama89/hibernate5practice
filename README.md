# Software Requirements

    docker   : https://docs.docker.com/docker-for-windows/install/
    jdk 1.8+ : https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
	maven    : https://maven.apache.org/download.cgi
	

# Installation
## Build source code 
    cd ${projectWorkingDir} > run mvn clean install
	    
## Start mysql/postgres database
    cd ${projectWorkingDir}/src/main/docker > run docker-compose up -d

