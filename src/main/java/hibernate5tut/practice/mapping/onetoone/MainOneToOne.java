package hibernate5tut.practice.mapping.onetoone;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.function.Function;


public class MainOneToOne {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainOneToOne.class);

    public static void main(String[] args) {
        MainOneToOne.saveWithSharePrimaryKey(DatabaseVendor.POSTGRES);
    }

    public static void saveWithSharePrimaryKey(DatabaseVendor vendor) {
        Function<Session, Serializable> func = session -> {
            Authority shipper = session.get(Authority.class, 1L);
            Account account = new Account();
            account.setUsername("Donald Trump");
            AccountDetails detail = new AccountDetails();
            detail.setAvatarUrl("http://avatar.com/001.png");
            detail.setPhoneNumber("001-888-900");
            detail.setAccount(account);
            account.setAccountDetail(detail);
            account.setAuthorities(Arrays.asList(shipper));
            return session.save(account);
        };
        Serializable createdAccount = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> new user: {}", createdAccount);
    }
}
