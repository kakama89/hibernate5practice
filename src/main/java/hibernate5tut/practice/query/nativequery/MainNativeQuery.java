package hibernate5tut.practice.query.nativequery;

import hibernate5tut.practice.query.hql.ClassAggDTO;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class MainNativeQuery {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainNativeQuery.class);

    public static void main(String[] args) {
        MainNativeQuery.getClazzNativeQueryAggregationResultTransform(DatabaseVendor.POSTGRES);
    }

    public static void getClazzNativeQueryAggregation(DatabaseVendor vendor) {
        Function<Session, List> func = session -> {
            String nativeQuery = "" +
                    " SELECT c.name as clazzName , count(s.*) as numberOfStudent , avg(s.resits) as avgResits, sum(s.resits) as sumResits, max(s.resits) as maxResits" +
                    " FROM tbl_clazz c INNER JOIN tbl_student s on c.id = s.clazz_id " +
                    " GROUP BY c.name HAVING avg(s.resits) > 1";

            Query query = session
                    .createNativeQuery(nativeQuery)
                    .addScalar("clazzName", StringType.INSTANCE)
                    .addScalar("numberOfStudent", LongType.INSTANCE)
                    .addScalar("avgResits", DoubleType.INSTANCE)
                    .addScalar("sumResits", LongType.INSTANCE)
                    .addScalar("maxResits", IntegerType.INSTANCE);
            List results = query.list();
            return results;
        };
        List result = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> result {}", result);
    }


    public static void getClazzNativeQueryAggregationResultTransform(DatabaseVendor vendor) {
        Function<Session, List<ClassAggDTO>> func = session -> {
            String nativeQuery = "" +
                    " SELECT c.name as clazzName , count(s.*) as numberOfStudent , avg(s.resits) as avgResits, sum(s.resits) as sumResits, max(s.resits) as maxResits" +
                    " FROM tbl_clazz c INNER JOIN tbl_student s on c.id = s.clazz_id " +
                    " GROUP BY c.name HAVING avg(s.resits) > 1";

            Query query = session
                    .createNativeQuery(nativeQuery)
                    .addScalar("clazzName", StringType.INSTANCE)
                    .addScalar("numberOfStudent", LongType.INSTANCE)
                    .addScalar("avgResits", DoubleType.INSTANCE)
                    .addScalar("sumResits", LongType.INSTANCE)
                    .addScalar("maxResits", IntegerType.INSTANCE);
            List<ClassAggDTO> results = query
                    .setResultTransformer(Transformers.aliasToBean(ClassAggDTO.class))
                    .list();
            return results;
        };
        List<ClassAggDTO> result = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> result {}", result);
    }
}
