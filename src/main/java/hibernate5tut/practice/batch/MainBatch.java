package hibernate5tut.practice.batch;

import hibernate5tut.practice.state.Person;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;

import java.util.List;
import java.util.function.Function;

public class MainBatch {
    public static void main(String[] args) {
//        MainBatch.insertWithBatch(DatabaseVendor.MYSQL, 1, 100, 20);
//        MainBatch.insertWithoutBatch(DatabaseVendor.POSTGRES, 1, 100);
//        MainBatch.updateWithBatch(DatabaseVendor.POSTGRES, 20);
        MainBatch.deleteWithBatch(DatabaseVendor.POSTGRES, 20);
    }

    // must enable jdbc.batch_size in config file or set manually for session
    // enable generate_statistics to view statistic
    public static void insertWithBatch(DatabaseVendor vendor, int start, int end, int batchSize) {
        Function<Session, Void> func = session -> {
//            session.setJdbcBatchSize(50);  // u can override config setting for this session only
            for (int i = start; i < end; i++) {
                Person person = new Person();
                person.setName("Name" + i);
                session.persist(person);
                if (i % batchSize == 0) {    // batch = min (jdbcBatchSize,  batchSize)
                    session.flush();
                    session.clear();
                }
            }
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    // disable jdbc.batch_size in config file and do not set session.setJdbcBatchSize
    // enable generate_statistics to view statistic
    public static void insertWithoutBatch(DatabaseVendor vendor, int start, int end) {
        Function<Session, Void> func = session -> {
            for (int i = start; i < end; i++) {
                Person person = new Person();
                person.setName("Name" + i);
                session.persist(person);
            }
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }


    public static void updateWithBatch(DatabaseVendor vendor, int batchSize) {
        Function<Session, List<Person>> func = session -> {
            List<Person> allPerson = session.createQuery("From Person ", Person.class).list();
            for (int i = 0; i < allPerson.size(); i++) {
                Person person = allPerson.get(i);
                person.setName(person.getName() + ":" + Math.random());
                session.update(person);
                if (i % batchSize == 0) {    // batch = min (jdbcBatchSize,  batchSize)
                    session.flush();
                    session.clear();
                }
            }
            return allPerson;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void deleteWithBatch(DatabaseVendor vendor, int batchSize) {
        Function<Session, List<Person>> func = session -> {
            List<Person> allPerson = session.createQuery("From Person ", Person.class).list();
            for (int i = 0; i < allPerson.size(); i++) {
                Person person = allPerson.get(i);
                session.delete(person);
                if (i % batchSize == 0) {    // batch = min (jdbcBatchSize,  batchSize)
                    session.flush();
                    session.clear();
                }
            }
            return allPerson;
        };
        HibernateUtils.execute(func, vendor);
    }
}
