package hibernate5tut.practice.inheritance.singletable;

import hibernate5tut.practice.inheritance.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_employee")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public abstract class AbstractEmployee extends BaseEntity<Long, String> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_employee_id_seq_generator")
    @SequenceGenerator(name = "tbl_employee_id_seq_generator", sequenceName = "tbl_employee_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Override
    public String toString() {
        return MessageFormat.format("AbstractEmployee[id {0}, name {1}]", id, name);
    }
}
