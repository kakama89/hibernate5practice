package hibernate5tut.practice.state;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Persistence {
    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;
//        Persistence.persistTransient(vendor);
//        Persistence.persistDetach(vendor, 1L);
//        Persistence.persistPersistenceObject(vendor, 1L);
//        Persistence.persistDetach2Context(vendor, 1L);
        Persistence.persistDetachContext2(vendor, 1L);
    }

    public static void persistTransient(DatabaseVendor vendor) {
        Person transientPerson = new Person();
        transientPerson.setName("Nguyen Thanh Tung");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.persist(transientPerson); // session.save will add transient object to persistent context
        tx.commit(); // tx.commit will apply the change to database
        session.close();
    }

    public static void persistDetach(DatabaseVendor vendor, Long id) { // throws PersistentObjectException : detached entity passed to persist
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        session.detach(persistence);
        persistence.setName("Detached..");
        session.persist(persistence);
        tx.commit();
        session.close();
    }

    public static void persistDetach2Context(DatabaseVendor vendor, Long id) { // throws PersistentObjectException : detached entity passed to persist
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        tx.commit();
        Session session2 = factory.openSession();
        Transaction tx2 = session.beginTransaction();
        persistence.setName("Persist from context 2");
        session2.persist(persistence);
        tx2.commit();
        session.close();
    }

    public static void persistDetachContext2(DatabaseVendor vendor, Long id) { // throws PersistentObjectException : detached entity passed to persist
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        Person detach = new Person();
        detach.setId(id);
        detach.setName("Detach 2");
        session.persist(detach);
        tx.commit();
        session.close();
    }

    public static void persistPersistenceObject(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        persistence.setName("Persist a persistence object");
        session.persist(persistence);
        tx.commit();
        session.close();
    }
}
