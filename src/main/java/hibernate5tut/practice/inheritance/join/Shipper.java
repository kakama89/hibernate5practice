package hibernate5tut.practice.inheritance.join;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_shipper")
@PrimaryKeyJoinColumn(name = "shipper_id")
public class Shipper extends AbstractUser {
    @Column(name = "output_limit", nullable = false)
    private boolean outputLimit;


    @Override
    public String toString() {
        return MessageFormat.format("Shipper[id {0}, name {1}, status {2}, outputLimit {3}]",
                getId(), getName(), getStatus(), outputLimit);
    }
}
