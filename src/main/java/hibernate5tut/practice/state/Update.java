package hibernate5tut.practice.state;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Update {
    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;
//        Update.updatePersistence(vendor);
//        Update.updateDetach(vendor, 1L);
//        Update.updateTransient(vendor, 1L);
        Update.updateDetach2(vendor, 1L);
    }

    public static void updateTransient(DatabaseVendor vendor) { // throws exception :The given object has a null identifier
        Person transientPerson = new Person();
        transientPerson.setName("Update a Transient");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(transientPerson);
        tx.commit();
        session.close();
    }

    // id not exist : throws StaleObjectStateException : Row was updated or deleted by another transaction
    // id  exist : update
    public static void updateTransient(DatabaseVendor vendor, Long id) {
        Person transientPerson = new Person();
        transientPerson.setId(id);
        transientPerson.setName("Update a Transient with not exist id");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.update(transientPerson);
        tx.commit();
        session.close();
    }

    public static void updateDetach(DatabaseVendor vendor, Long id) { // will update new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        session.evict(persistence);
        Person detach = persistence;
        detach.setName("Update a detach object -> create new ");
        session.update(detach);
        tx.commit();
        session.close();
    }

    public static void updateDetach2(DatabaseVendor vendor, Long id) { // will update new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        Person detach = new Person();
        detach.setId(1L);
        detach.setName("Update a detach object -> create new ");
        session.update(detach);
        tx.commit();
        session.close();
    }

    public static void updatePersistence(DatabaseVendor vendor, Long id) { // will update persistence record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, id);
        persistence.setName("Update a persistence object -> update");
        session.update(persistence);
        tx.commit();
        session.close();
    }
}
