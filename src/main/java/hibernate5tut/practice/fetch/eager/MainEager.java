package hibernate5tut.practice.fetch.eager;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;
import java.util.function.Function;

/**
 * {@link Product#company}
 * {@link Company#products}
 */

public class MainEager {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainEager.class);

    public static void main(String[] args) {
        MainEager.loadEntityAlwaysLoadAssociation(DatabaseVendor.MYSQL, 1L);
    }

    /**
     * Hibernate will fetch entity and eager associations at the same time
     *
     * @param vendor : Database vender
     * @param pk     : primary key
     */
    public static void loadEntityAlwaysLoadAssociation(DatabaseVendor vendor, Long pk) {
        Function<Session, Company> func = session -> session.get(Company.class, pk);
        Company company = HibernateUtils.execute(func, vendor);
        MainEager.printOut(company);
    }


    /**
     * When we mask fetch = FetchType.EAGER
     * the association always loaded even we try
     * to override fetch mode to SELECT (LAZY)
     *
     * @param vendor
     * @param pk
     */
    public static void forceToOverideFetchModeFailure(DatabaseVendor vendor, Long pk) {
        Function<Session, Company> func = session -> {
            Criteria criteria = session.createCriteria(Company.class);
            criteria.setFetchMode(Company_.PRODUCTS, FetchMode.SELECT);
            criteria.add(Restrictions.eq(Company_.ID, pk));
            Company company = (Company) criteria.list().get(0);
            return company;
        };
        Company company = HibernateUtils.execute(func, vendor);
        MainEager.printOut(company);
    }


    private static void printOut(Company company) {
        Set<Product> products = company.getProducts();
        LOGGER.debug(">>>>>>> Company {}, products : {}", company.getName(), products);
    }
}
