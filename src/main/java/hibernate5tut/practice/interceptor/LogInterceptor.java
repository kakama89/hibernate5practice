package hibernate5tut.practice.interceptor;

import hibernate5tut.practice.mapping.onetoone.Account;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Use to inspect / alter object
 */
public class LogInterceptor extends EmptyInterceptor {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogInterceptor.class);

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        if (entity instanceof Account) {
            LOGGER.error(">>> on save user : {}", entity);
        }
        return super.onSave(entity, id, state, propertyNames, types);
    }
}
