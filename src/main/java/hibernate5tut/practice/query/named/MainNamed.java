package hibernate5tut.practice.query.named;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class MainNamed {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainNamed.class);

    public static void main(String[] args) {
        MainNamed.findSiteByTimeZone(DatabaseVendor.MYSQL, "Asia/Bangkok");
        // MainNamed.findClientByNameLike(DatabaseVendor.MYSQL, "VN");
    }

    /**
     * Select client by name like
     * use NamedQuery
     *
     * @param vendor
     * @param name
     */
    public static void findClientByNameLike(DatabaseVendor vendor, String name) {
        Function<Session, List<Client>> func = session ->
                session.createNamedQuery("Client.findByNameLike", Client.class)
                        .setParameter("name", '%' + name + '%')
                        .list();
        List<Client> clients = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> Clients : {}", clients);
    }

    /**
     * Select site by timezone
     * use NamedNativeQuery
     *
     * @param vendor
     * @param timezone
     */
    public static void findSiteByTimeZone(DatabaseVendor vendor, String timezone) {
        Function<Session, List<Site>> func = session ->
                session.createNamedQuery("Site.findByTimeZone", Site.class)
                        .setParameter("timezone", timezone)
                        .list();
        List<Site> clients = HibernateUtils.execute(func, vendor);
        LOGGER.error(">> Sites : {}", clients);
    }
}
