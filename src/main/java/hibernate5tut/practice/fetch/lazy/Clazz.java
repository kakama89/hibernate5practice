package hibernate5tut.practice.fetch.lazy;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "tbl_clazz")
@NamedEntityGraph(name = "Clazz.students", attributeNodes = @NamedAttributeNode(Clazz_.STUDENTS))
public class Clazz {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_clazz_id_seq_generator")
    @SequenceGenerator(name = "tbl_clazz_id_seq_generator", sequenceName = "tbl_clazz_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "clazz", fetch = FetchType.LAZY)
//    @BatchSize(size = 5)
    private Set<Student> students;

    @Override
    public String toString() {
        return MessageFormat.format("\nClazz[id {0}, name {1}]", id, name);
    }
}
