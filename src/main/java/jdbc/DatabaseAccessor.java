package jdbc;

import hibernate5tut.utils.DatabaseVendor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseAccessor {

    private Connection getConnection(String driverName, String url, String user, String pass) {
        Connection connection = null;
        try {
            Class.forName(driverName);
            connection = DriverManager.getConnection(url, user, pass);
        } catch (Exception e) {
            System.err.println("Cannot create connection");
            e.printStackTrace();
        }
        return connection;
    }

    public void release(Connection connection) {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            System.err.println("Cannot release connection");
            e.printStackTrace();
        }
    }


    public Connection getConnection(DatabaseVendor vendor) {
        if (DatabaseVendor.MYSQL.equals(vendor)) {
            return getConnection("com.mysql.cj.jdbc.Driver", "jdbc:mysql://localhost:3308/practice_mysql?useSSL=false", "root", "root");
        }
        return getConnection("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/practice_postgres?rewriteBatchedStatements=true", "root", "root");
    }
}
