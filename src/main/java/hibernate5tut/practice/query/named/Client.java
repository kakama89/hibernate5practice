package hibernate5tut.practice.query.named;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_client")
@NamedQuery(name = "Client.findByNameLike", query = "From Client c where c.name like :name")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_client_id_seq_generator")
    @SequenceGenerator(name = "tbl_client_id_seq_generator", sequenceName = "tbl_client_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "alias", unique = true)
    private String alias;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "site_id", nullable = false)
    private Site site;

    @Override
    public String toString() {
        return MessageFormat.format("\nClient[id {0}, name {1}, alias {2}]", id, name, alias);
    }
}
