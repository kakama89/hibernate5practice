package hibernate5tut.practice.inheritance.join;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;


@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_user")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_user_id_seq_generator")
    @SequenceGenerator(name = "tbl_user_id_seq_generator", sequenceName = "tbl_user_id_seq", allocationSize = 1)
    private Long id;

    @Basic(optional = false)
    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private UserStatus status;

    @Override
    public String toString() {
        return MessageFormat.format("AbstractUser[id {0}, name {1}, status {2}]", id, name, status);
    }
}
