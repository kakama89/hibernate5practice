package hibernate5tut.practice.inheritance.tableperclass;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tbl_rectangle")
public class Rectangle extends Shape {

    @Column(name = "width")
    private Long width;

    @Column(name = "height")
    private Long height;
}
