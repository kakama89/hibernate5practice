package hibernate5tut.practice.inheritance;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity<T extends Serializable, U> {
    @Id
    @Column(updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private T id;

    @Column(name = "created_by")
    protected U createdBy;

    @Column(name = "created_date")
    protected Instant createdDate;

    @Column(name = "last_modified_by")
    protected U lastModifiedBy;

    @Column(name = "last_modified_date")
    protected Instant lastModifiedDate;

    @Version
    @Column(name = "version")
    private Long version;
}
