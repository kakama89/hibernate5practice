package hibernate5tut.practice.inheritance.singletable;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;

public class MainSingleTable {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainSingleTable.class);

    public static void main(String[] args) {
        MainSingleTable.createContractEmployee(DatabaseVendor.MYSQL, "Kien", Instant.now().plus(30, ChronoUnit.DAYS));
//        MainSingleTable.createPermanentEmployee(DatabaseVendor.MYSQL, "Khoa", 1);
//        MainSingleTable.getContractEmployee(DatabaseVendor.MYSQL, AbstractEmployee.class);
//        MainSingleTable.getContractEmployee(DatabaseVendor.MYSQL, PermanentEmployee.class);
//        MainSingleTable.getContractEmployee(DatabaseVendor.MYSQL, ContractEmployee.class);
    }

    public static <T extends AbstractEmployee> void getContractEmployee(DatabaseVendor databaseVendor, Class<T> clazz) {
        Function<Session, List<T>> func = session -> {
            List<T> employees = session.createQuery("FROM " + clazz.getName(), clazz)
                    .list();
            return employees;
        };
        List<T> employees = HibernateUtils.execute(func, databaseVendor);
        LOGGER.error(">>> Employees : {}", employees);
    }

    public static void createContractEmployee(DatabaseVendor databaseVendor, String name, Instant endDate) {
        Function<Session, Void> func = session -> {
            ContractEmployee employee = new ContractEmployee();
            employee.setName(name);
            employee.setEndDate(endDate);
            session.persist(employee);
            return (Void) null;
        };
        HibernateUtils.execute(func, databaseVendor);
    }

    public static void createPermanentEmployee(DatabaseVendor databaseVendor, String name, Integer healthPackages) {
        Function<Session, Void> func = session -> {
            PermanentEmployee employee = new PermanentEmployee();
            employee.setName(name);
            employee.setHealthPackages(healthPackages);
            session.persist(employee);
            return (Void) null;
        };
        HibernateUtils.execute(func, databaseVendor);
    }

}
