package hibernate5tut.practice.cache;

import hibernate5tut.practice.mapping.recursive.Menu;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.CacheMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class L2Cache {
    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.POSTGRES;
        L2Cache.canGetFromSecondLevelCache(vendor, 1L);
//        L2Cache.forceSessionIgnoreCache(vendor, 1L);
//        L2Cache.clearEntityFromL2Cache(vendor, 1L);
    }

    // There is only 1 hit to database for 2 queries created by 2 difference sessions.
    public static void canGetFromSecondLevelCache(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session firstSession = factory.openSession();
        Transaction tx = firstSession.beginTransaction();
        Menu firstMenu = firstSession.get(Menu.class, id);
        tx.commit();
        firstSession.close();

        Session secondSession = factory.openSession();
        Transaction tx2 = secondSession.beginTransaction();
        // This query first get from l1 cache and it does not find entity from cache
        // Then it will try to get from l2 cache and found the entity
        // so it won't hit to database
        Menu secondMenu = secondSession.get(Menu.class, id);
        tx2.commit();
        secondSession.close();
    }


    public static void forceSessionIgnoreCache(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session firstSession = factory.openSession();
        firstSession.setCacheMode(CacheMode.IGNORE); // tell session 1 ignore the cache
        Transaction tx = firstSession.beginTransaction();
        Menu firstMenu = firstSession.get(Menu.class, id);
        tx.commit();
        firstSession.close();

        Session secondSession = factory.openSession();
        Transaction tx2 = secondSession.beginTransaction();
        Menu secondMenu = secondSession.get(Menu.class, id);
        tx2.commit();
        secondSession.close();
    }

    public static void clearEntityFromL2Cache(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session firstSession = factory.openSession();
        Transaction tx = firstSession.beginTransaction();
        Menu firstMenu = firstSession.get(Menu.class, id);
        tx.commit();
        firstSession.close();

        factory.getCache().evict(Menu.class, id); // tell session factory to clear entity from cache so session 2 won't get from cache.

        Session secondSession = factory.openSession();
        Transaction tx2 = secondSession.beginTransaction();

        Menu secondMenu = secondSession.get(Menu.class, id);
        tx2.commit();
        secondSession.close();
    }
}
