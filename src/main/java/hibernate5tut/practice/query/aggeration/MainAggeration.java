package hibernate5tut.practice.query.aggeration;

import hibernate5tut.utils.DatabaseVendor;

/**
 * {@link hibernate5tut.practice.query.hql.MainHQL#getAllClassAggregation(DatabaseVendor)}
 * {@link hibernate5tut.practice.query.criteria.MainHibernateCriteria#withGroupByAndHaving(DatabaseVendor)}
 * {@link hibernate5tut.practice.query.criteria.MainHibernateCriteria#withGroupBy(DatabaseVendor)}
 * {@link hibernate5tut.practice.query.criteria.MainJPACriteriaBuilder#useProjectionWithGroupByAndHaving(DatabaseVendor)}
 * {@link hibernate5tut.practice.query.nativequery.MainNativeQuery#getClazzNativeQueryAggregation(DatabaseVendor)}
 * {@link hibernate5tut.practice.query.hql.MainHQL#getAllClassAggregation(DatabaseVendor)}
 */
public class MainAggeration {
}
