package hibernate5tut.practice.mapping.primary;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
@Embeddable
public class UserResourceActionId implements Serializable {
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "resource_id")
    private Long resourceId;

    @Column(name = "action_id")
    private Long actionId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResourceActionId that = (UserResourceActionId) o;
        return Objects.equals(userId, that.userId) &&
                Objects.equals(resourceId, that.resourceId) &&
                Objects.equals(actionId, that.actionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, resourceId, actionId);
    }


    @Override
    public String toString() {
        return MessageFormat.format("\nUserResourceActionId[userId {0}, resourceId {1}, actionId {2}]", userId, resourceId, actionId);
    }
}
