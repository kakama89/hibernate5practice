package hibernate5tut.practice.fetch.eager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_product")
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_product_id_seq_generator")
    @SequenceGenerator(name = "tbl_product_id_seq_generator", sequenceName = "tbl_product_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "code", unique = true, nullable = false, length = 10)
    private String code;


    @ManyToOne// EAGER is default for OneToOne or ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    public String toString() {
        return MessageFormat.format("Product[id {0}, code {1}]", id, code);
    }
}
