package hibernate5tut.practice.mapping.manytoone;

/**
 * Mapping
 * {@link hibernate5tut.practice.query.named.Client#site}
 * {@link hibernate5tut.practice.fetch.eager.Product#company}
 * {@link hibernate5tut.practice.fetch.lazy.Student#clazz}
 * {@link hibernate5tut.practice.mapping.recursive.Menu#parent}
 * {@link hibernate5tut.practice.inheritance.join.Sender#shipper}
 */
public class MainManyToOne {
}
