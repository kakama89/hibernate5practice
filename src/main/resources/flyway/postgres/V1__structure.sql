SET SCHEMA 'public';
-- structure for tbl_clazz
CREATE SEQUENCE tbl_clazz_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_clazz (
    id bigint NOT NULL DEFAULT nextval('tbl_clazz_id_seq'::regclass),
    name character varying(255) NOT NULL
);
ALTER SEQUENCE tbl_clazz_id_seq OWNED BY tbl_clazz.id;
ALTER TABLE ONLY tbl_clazz ADD CONSTRAINT PK_tbl_clazz__id PRIMARY KEY (id);


-- structure for tbl_student
CREATE SEQUENCE tbl_student_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_student (
    id bigint NOT NULL DEFAULT nextval('tbl_student_id_seq'::regclass),
    name character varying(255) NOT NULL,
    clazz_id bigint NOT NULL,
    resits int not null default 0
);
ALTER SEQUENCE tbl_clazz_id_seq OWNED BY tbl_student.id;
ALTER TABLE ONLY tbl_student ADD CONSTRAINT PK_tbl_student__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_student ADD CONSTRAINT FK_tbl_student_tbl_clazz__clazz_id FOREIGN KEY (clazz_id) REFERENCES tbl_clazz(id);

-- structure for tbl_company
CREATE SEQUENCE tbl_company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_company (
    id bigint NOT NULL DEFAULT nextval('tbl_company_id_seq'::regclass),
    name character varying(255) NOT NULL
);
ALTER SEQUENCE tbl_company_id_seq OWNED BY tbl_company.id;
ALTER TABLE ONLY tbl_company ADD CONSTRAINT PK_tbl_company__id PRIMARY KEY (id);

-- structure for tbl_product
CREATE SEQUENCE tbl_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_product (
    id bigint NOT NULL DEFAULT nextval('tbl_product_id_seq'::regclass),
    code character varying(10) NOT NULL ,
    company_id bigint NOT NULL
);
ALTER SEQUENCE tbl_product_id_seq OWNED BY tbl_product.id;
ALTER TABLE ONLY tbl_product ADD CONSTRAINT PK_tbl_product__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_product ADD CONSTRAINT FK_tbl_product_tbl_company__company_id FOREIGN KEY (company_id) REFERENCES tbl_company(id);
ALTER TABLE ONLY tbl_product ADD CONSTRAINT UK_tbl_product__code UNIQUE(code);

-- structure for tbl_person
CREATE SEQUENCE tbl_person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_person (
    id bigint NOT NULL DEFAULT nextval('tbl_person_id_seq'::regclass),
    name character varying(255) NOT NULL
);
ALTER SEQUENCE tbl_person_id_seq OWNED BY tbl_person.id;
ALTER TABLE ONLY tbl_person ADD CONSTRAINT PK_tbl_person__id PRIMARY KEY (id);


-- structure for tbl_account
CREATE SEQUENCE tbl_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_account (
    id bigint NOT NULL DEFAULT nextval('tbl_account_id_seq'::regclass),
    username character varying(255) NOT NULL
);
ALTER SEQUENCE tbl_account_id_seq OWNED BY tbl_account.id;
ALTER TABLE ONLY tbl_account ADD CONSTRAINT PK_tbl_account__id PRIMARY KEY (id);

-- structure for tbl_account_detail
CREATE TABLE tbl_account_detail (
    account_id bigint NOT NULL,
    avatar_url character varying(255) NOT NULL,
    phone_number character varying(20) NOT NULL
);
ALTER TABLE ONLY tbl_account_detail ADD CONSTRAINT PK_tbl_account_detail__id PRIMARY KEY (account_id);
ALTER TABLE ONLY tbl_account_detail ADD CONSTRAINT FK_tbl_account_detail_tbl_account__account_id FOREIGN KEY (account_id) REFERENCES tbl_account(id);

-- create hibernate sequence
CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

-- structure for tbl_authority
CREATE SEQUENCE tbl_authority_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_authority (
    id bigint NOT NULL DEFAULT nextval('tbl_authority_id_seq'::regclass),
    name character varying(100) NOT NULL
);
ALTER SEQUENCE tbl_authority_id_seq OWNED BY tbl_authority.id;
ALTER TABLE ONLY tbl_authority ADD CONSTRAINT PK_tbl_authority__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_authority ADD CONSTRAINT UK_tbl_authority__name UNIQUE(name);


CREATE TABLE tbl_account_authority (
    account_id bigint NOT NULL,
    authority_id bigint NOT NULL
);
ALTER TABLE ONLY tbl_account_authority ADD CONSTRAINT PK_tbl_account_authority__account_id__authority_id PRIMARY KEY (account_id, authority_id);
ALTER TABLE ONLY tbl_account_authority ADD CONSTRAINT FK_tbl_account_authority_tbl_account__account_id FOREIGN KEY (account_id) REFERENCES tbl_account(id);
ALTER TABLE ONLY tbl_account_authority ADD CONSTRAINT FK_tbl_account_authority_tbl_authority__authority_id FOREIGN KEY (authority_id) REFERENCES tbl_authority(id);

-- structure for tbl_menu
CREATE SEQUENCE tbl_menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_menu (
    id bigint NOT NULL DEFAULT nextval('tbl_menu_id_seq'::regclass),
    name character varying(255) NOT NULL,
    parent_id bigint NULL,
    priority int not null default 0
);
ALTER SEQUENCE tbl_menu_id_seq OWNED BY tbl_menu.id;
ALTER TABLE ONLY tbl_menu ADD CONSTRAINT PK_tbl_menu__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_menu ADD CONSTRAINT FK_tbl_menu_tbl_menu__parent_id FOREIGN KEY (parent_id) REFERENCES tbl_menu(id);


-- structure for tbl_site
CREATE SEQUENCE tbl_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_site (
    id bigint NOT NULL DEFAULT nextval('tbl_site_id_seq'::regclass),
    alias character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    time_zone character varying(20) NULL
);
ALTER SEQUENCE tbl_site_id_seq OWNED BY tbl_site.id;
ALTER TABLE ONLY tbl_site ADD CONSTRAINT PK_tbl_site__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_site ADD CONSTRAINT UK_tbl_site__alias UNIQUE(alias);


-- structure for tbl_client
CREATE SEQUENCE tbl_client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_client (
    id bigint NOT NULL DEFAULT nextval('tbl_client_id_seq'::regclass),
    alias character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    site_id bigint NOT NULL
);
ALTER SEQUENCE tbl_client_id_seq OWNED BY tbl_client.id;
ALTER TABLE ONLY tbl_client ADD CONSTRAINT PK_tbl_client__id PRIMARY KEY (id);
ALTER TABLE ONLY tbl_client ADD CONSTRAINT UK_tbl_client__alias UNIQUE(alias);
ALTER TABLE ONLY tbl_client ADD CONSTRAINT FK_tbl_client_tbl_site__site_id FOREIGN KEY (site_id) REFERENCES tbl_site(id);

-- structure for tbl_user_resource_action
CREATE TABLE tbl_user_resource_action (
    user_id bigint NOT NULL,
    resource_id bigint NOT NULL,
    action_id bigint NOT NULL,
    description character varying(100) NULL,
    status character varying(20) NULL
);
ALTER TABLE ONLY tbl_user_resource_action 
ADD CONSTRAINT PK_tbl_user_resource_action__user_id__resource_id__action_id PRIMARY KEY (user_id, resource_id,action_id);


-- structure for tbl_employee
CREATE SEQUENCE tbl_employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_employee(
  id bigint NOT NULL DEFAULT nextval('tbl_employee_id_seq'::regclass) ,
  name character varying(100) NOT NULL,
  type character varying(10) NOT NULL,
  health_packages int NULL,
  end_date timestamp NULL,
  created_date timestamp null,
  created_by character varying(255) null,
  last_modified_date timestamp null,
  last_modified_by character varying(255) null,
  version bigint not null default 1
);

ALTER SEQUENCE tbl_employee_id_seq OWNED BY tbl_employee.id;
ALTER TABLE ONLY tbl_employee ADD CONSTRAINT PK_tbl_employee__id PRIMARY KEY (id);

-- structure for tbl_user
CREATE SEQUENCE tbl_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
CREATE TABLE tbl_user(
  id bigint NOT NULL DEFAULT nextval('tbl_user_id_seq'::regclass) ,
  name character varying(255) NOT NULL,
  status character varying(10) NOT NULL
);

ALTER SEQUENCE tbl_user_id_seq OWNED BY tbl_user.id;
ALTER TABLE ONLY tbl_user ADD CONSTRAINT PK_tbl_user__id PRIMARY KEY (id);


-- structure for tbl_shipper
CREATE TABLE tbl_shipper(
  shipper_id bigint NOT NULL ,
  output_limit boolean NULL
) ;

ALTER TABLE ONLY tbl_shipper ADD CONSTRAINT PK_tbl_shipper__shipper_id PRIMARY KEY (shipper_id);
ALTER TABLE ONLY tbl_shipper ADD CONSTRAINT FK_tbl_shipper_tbl_user__shipper_id FOREIGN KEY (shipper_id) REFERENCES tbl_user(id);

-- structure for tbl_sender
CREATE TABLE tbl_sender(
  sender_id bigint NOT NULL ,
  card_id character varying(255) NULL,
  tax_id character varying(255) NULL,
  email character varying(255) NULL,
  shipper_id bigint NULL
) ;

ALTER TABLE ONLY tbl_sender ADD CONSTRAINT PK_tbl_sender__sender_id PRIMARY KEY (sender_id);
ALTER TABLE ONLY tbl_sender ADD CONSTRAINT FK_tbl_sender_tbl_user__sender_id FOREIGN KEY (sender_id) REFERENCES tbl_user(id);
ALTER TABLE ONLY tbl_sender ADD CONSTRAINT FK_tbl_sender_tbl_shipper__shipper_id FOREIGN KEY (shipper_id) REFERENCES tbl_shipper(shipper_id);


-- structure for tbl_receiver
CREATE TABLE tbl_receiver(
  receiver_id bigint NOT NULL ,
  province_id bigint NULL,
  remote_area_fee bigint NULL
);

ALTER TABLE ONLY tbl_receiver ADD CONSTRAINT PK_tbl_receiver__receiver_id PRIMARY KEY (receiver_id);
ALTER TABLE ONLY tbl_receiver ADD CONSTRAINT FK_tbl_receiver_tbl_user__receiver_id FOREIGN KEY (receiver_id) REFERENCES tbl_user(id);



-- structure for tbl_shape
CREATE SEQUENCE tbl_shape_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tbl_shape (
	id bigint NOT NULL DEFAULT nextval('tbl_shape_id_seq'::regclass) ,
	shape_name varchar(255) NOT
	NULL
);
ALTER TABLE ONLY tbl_shape ADD CONSTRAINT PK_tbl_shape__id PRIMARY KEY (id);
-- structure for tbl_circle

CREATE TABLE tbl_circle (
	id bigint NOT NULL DEFAULT nextval('tbl_shape_id_seq'::regclass) ,
	shape_name varchar(255) NOT NULL,
	radius bigint NOT NULL
);
ALTER TABLE ONLY tbl_circle ADD CONSTRAINT PK_tbl_circle__id PRIMARY KEY (id);
-- structure for tbl_rectangle
CREATE TABLE tbl_rectangle (
	id int8 NOT NULL DEFAULT nextval('tbl_shape_id_seq'::regclass) ,
	shape_name varchar(255) NOT NULL,
	height int8 NOT NULL,
	width int8 NOT NULL
);
ALTER TABLE ONLY tbl_rectangle ADD CONSTRAINT PK_tbl_rectangle__id PRIMARY KEY (id);