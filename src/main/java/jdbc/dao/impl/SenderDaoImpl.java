package jdbc.dao.impl;

import hibernate5tut.practice.inheritance.join.Sender;
import hibernate5tut.practice.inheritance.join.Shipper;
import hibernate5tut.practice.inheritance.join.UserStatus;
import jdbc.dao.dao.SenderDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SenderDaoImpl implements SenderDao {
    private final Connection connection;

    public SenderDaoImpl(Connection connection) {
        this.connection = connection;

    }

    @Override
    public List<Sender> getAllSender() throws SQLException {
        Statement stmt = connection.createStatement();
        String sql = "SELECT * FROM tbl_user u INNER JOIN tbl_sender s ON u.id = s.sender_id";
        ResultSet rs = stmt.executeQuery(sql);
        List<Sender> senders = new ArrayList<>();
        while(rs.next()){
            Sender sender = new Sender();
            sender.setId(rs.getLong("id"));
            sender.setName(rs.getString("name"));
            sender.setStatus(UserStatus.valueOf(rs.getString("status")));
            sender.setCardId(rs.getString("card_id"));
            sender.setTaxId(rs.getString("tax_id"));
            sender.setEmail(rs.getString("email"));

            // create query to get shipper by id
            PreparedStatement pstm = connection.prepareStatement("SELECT * FROM tbl_user u INNER JOIN tbl_shipper p ON u.id = p.shipper_id WHERE p.shipper_id = ?");
            long shipperId = rs.getLong("shipper_id");
            pstm.setLong(1  , shipperId);
            ResultSet rs2 = pstm.executeQuery();
            
            if (rs2.next()) {
                Shipper shipper = new Shipper();
                shipper.setId(rs2.getLong("id"));
                shipper.setName(rs2.getString("name"));
                shipper.setOutputLimit(rs2.getBoolean("output_limit"));
                sender.setShipper(shipper); // SET shipper
            }

            senders.add(sender);
        }
        return senders;
    }
}
