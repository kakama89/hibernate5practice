package hibernate5tut.practice.query.named;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tbl_site")
@NamedNativeQuery(name = "Site.findByTimeZone", query = "SELECT s.* FROM tbl_site s WHERE s.time_zone = :timezone", resultClass = Site.class)
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_site_id_seq_generator")
    @SequenceGenerator(name = "tbl_site_id_seq_generator", sequenceName = "tbl_site_id_seq", allocationSize = 1)

    private Long id;

    @Column(name = "alias", unique = true)
    private String alias;

    @Column(name = "name")
    private String name;

    @Column(name = "time_zone")
    private String timezone;

    @OneToMany(mappedBy = "site", fetch = FetchType.LAZY)
    private List<Client> clients;

    @Override
    public String toString() {
        return MessageFormat.format("\nSite[id {0}, name {1}, alias {2}, timezone {3}]", id, name, alias, timezone);
    }
}
