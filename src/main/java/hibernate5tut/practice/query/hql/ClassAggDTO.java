package hibernate5tut.practice.query.hql;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.text.MessageFormat;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ClassAggDTO {
    private String clazzName;
    private Long numberOfStudent;
    private Double avgResits;
    private Long sumResits;
    private Integer maxResits;

    @Override
    public String toString() {
        return MessageFormat.format("\nClassAggDTO[clazzName {0}, numberOfStudent {1}, avgResits {2},  sumResits {3}, maxResits {4}",
                clazzName, numberOfStudent, avgResits, sumResits, maxResits);
    }

}
