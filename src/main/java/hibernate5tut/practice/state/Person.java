package hibernate5tut.practice.state;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_person")
public class Person {

    @Id
    @SequenceGenerator(name = "tbl_person_id_seq_generator", sequenceName = "tbl_person_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_person_id_seq_generator")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Override
    public String toString() {
        return MessageFormat.format("\nPerson[id {0}, name {1}]", id, name);
    }

//    @PrePersist
//    public void beforePersist() {
//        System.err.println("PrePersist : " + this);
//    }
//
//    @PostPersist
//    public void afterPersist() {
//        System.err.println("PostPersist: " + this);
//    }
//
//    @PreUpdate
//    public void preUpdate() {
//        System.err.println("PreUpdate: " + this);
//    }
//
//    @PostUpdate
//    public void postUpdate() {
//        System.err.println("PostUpdate: " + this);
//    }
}
