package hibernate5tut.practice.state;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Merge {
    public static void main(String[] args) {
//        Merge.mergeDetach(DatabaseVendor.MYSQL);
        Merge.mergeDetach2(DatabaseVendor.MYSQL);
    }

    public static void mergeTransient(DatabaseVendor vendor) { // will insert new record
        Person transientPerson = new Person();
        transientPerson.setName("Merge transient");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.merge(transientPerson);
        tx.commit();
        session.close();
    }

    public static void mergeDetach(DatabaseVendor vendor) { // will insert new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        session.evict(persistence);
        Person detach = persistence;
        detach.setName("Merge a detach object");
        session.merge(detach);
        tx.commit();
        session.close();
    }

    public static void mergeDetach2(DatabaseVendor vendor) { // will insert new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        Person detach = new Person();
        detach.setId(1000L);
        detach.setName("Merge a detach object 2");
        session.merge(detach);
        tx.commit();
        session.close();
    }

    public static void mergePersistence(DatabaseVendor vendor) { // will insert new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        session.evict(persistence);
        Person detach = persistence;
        detach.setName("Save a detach object");
        session.save(detach);
        tx.commit();
        session.close();
    }
}
