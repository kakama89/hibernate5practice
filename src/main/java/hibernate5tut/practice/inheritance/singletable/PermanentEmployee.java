package hibernate5tut.practice.inheritance.singletable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.text.MessageFormat;

@Getter
@Setter
@NoArgsConstructor
@Entity
@DiscriminatorValue(value = "PERMANENT")
public class PermanentEmployee extends AbstractEmployee {
    @Column(name = "health_packages")
    private Integer healthPackages;

    @Override
    public String toString() {
        return MessageFormat.format("PermanentEmployee[id {0}, name {1}, healthPackages {2}]", getId(), getName(), healthPackages);
    }
}
