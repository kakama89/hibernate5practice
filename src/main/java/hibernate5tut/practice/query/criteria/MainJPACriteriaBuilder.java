package hibernate5tut.practice.query.criteria;

import hibernate5tut.practice.fetch.lazy.Clazz;
import hibernate5tut.practice.fetch.lazy.Clazz_;
import hibernate5tut.practice.fetch.lazy.Student;
import hibernate5tut.practice.fetch.lazy.Student_;
import hibernate5tut.practice.query.paging.MainPaging;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.function.Function;

public class MainJPACriteriaBuilder {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainPaging.class);

    public static void main(String[] args) {
        MainJPACriteriaBuilder.useProjectionWithGroupByAndHaving(DatabaseVendor.POSTGRES);
    }

    /**
     * Get all classes which id > 5 order by class name
     * selectAllCompany * from class c inner join student s on c.id = s.clazz_id where c.id > 5 order by class name asc
     *
     * @param vendor
     * @return
     */
    public static List<Clazz> getDataUsingJPACriteriaBuilder(DatabaseVendor vendor) {
        Function<Session, List<Clazz>> func = session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Clazz> criteriaQuery = builder.createQuery(Clazz.class);
            Root<Clazz> root = criteriaQuery.from(Clazz.class);
            SetJoin<Clazz, Student> student = root.join(Clazz_.students, JoinType.INNER);
            criteriaQuery
                    .select(root)
                    .where(builder.greaterThan(root.get(Clazz_.ID), 5))
                    .orderBy(builder.asc(root.get(Clazz_.name)));
            return session.createQuery(criteriaQuery)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .list();
        };
        List<Clazz> classes = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> classes  : {}", classes);
        return classes;
    }


    /**
     * Get all classes which have at lest 1 student with non null name
     * selectAllCompany * from class c where id in (
     * selectAllCompany clazz_id from student where name is not null
     * )
     * order by c.name desc
     *
     * @param vendor
     * @return
     */
    public static List<Clazz> getDataUsingSubSelect(DatabaseVendor vendor) {
        Function<Session, List<Clazz>> func = session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Clazz> criteriaQuery = builder.createQuery(Clazz.class);
            Root<Clazz> root = criteriaQuery.from(Clazz.class);
            Subquery<Student> subQuery = criteriaQuery.subquery(Student.class);
            Root<Student> subRoot = subQuery.from(Student.class);
            subQuery.select(subRoot).where(builder.isNotNull(subRoot.get(Student_.NAME)));
            criteriaQuery
                    .select(root)
                    .where(builder.in(root.get(Clazz_.ID)).value(subQuery))
                    .orderBy(builder.desc(root.get(Clazz_.name)));
            return session.createQuery(criteriaQuery)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .list();
        };
        List<Clazz> classes = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> classes  : {}", classes);
        return classes;
    }

    /**
     * Get student group by clazz_id having clazz_id > 5 order by clazz_id asc
     * selectAllCompany clazz_id, count(*)
     * from student
     * group by clazz_id
     * having clazz_id > 5
     * order by clazz_id asc
     *
     * @param vendor
     * @return
     */
    public static List<StudentDTO> useProjectionWithGroupByAndHaving(DatabaseVendor vendor) {
        Function<Session, List<StudentDTO>> func = session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<StudentDTO> criteriaQuery = builder.createQuery(StudentDTO.class);
            Root<Student> root = criteriaQuery.from(Student.class);
            CriteriaQuery<StudentDTO> query = criteriaQuery
                    .multiselect(
                            root.get(Student_.CLAZZ).get(Clazz_.ID),
                            builder.count(root).alias("count"),
                            builder.avg(root.get(Student_.resits)).alias("resitsAvg"))
                    .groupBy(root.get(Student_.CLAZZ).get(Clazz_.ID))
                    .having(builder.lessThan(root.get(Student_.CLAZZ).get(Clazz_.ID), 5))
                    .orderBy(builder.asc(root.get(Student_.CLAZZ).get(Clazz_.ID)));
            List<StudentDTO> students = session.createQuery(query).getResultList();
            return students;
        };
        List<StudentDTO> students = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> students  : {}", students);
        return students;
    }
}
