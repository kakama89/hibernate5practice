package hibernate5tut.practice.listener;

import org.hibernate.HibernateException;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.event.spi.SaveOrUpdateEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * to listen events
 */
public class AuditLogListener implements SaveOrUpdateEventListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuditLogListener.class);

    @Override
    public void onSaveOrUpdate(SaveOrUpdateEvent saveOrUpdateEvent) throws HibernateException {

        LOGGER.error(">> ---------------------------------");
    }

}
