package hibernate5tut.utils;

import hibernate5tut.practice.fetch.eager.Company;
import hibernate5tut.practice.fetch.eager.Product;
import hibernate5tut.practice.fetch.lazy.Clazz;
import hibernate5tut.practice.fetch.lazy.Student;
import hibernate5tut.practice.inheritance.join.AbstractUser;
import hibernate5tut.practice.inheritance.join.Receiver;
import hibernate5tut.practice.inheritance.join.Sender;
import hibernate5tut.practice.inheritance.join.Shipper;
import hibernate5tut.practice.inheritance.singletable.AbstractEmployee;
import hibernate5tut.practice.inheritance.singletable.ContractEmployee;
import hibernate5tut.practice.inheritance.singletable.PermanentEmployee;
import hibernate5tut.practice.inheritance.tableperclass.Circle;
import hibernate5tut.practice.inheritance.tableperclass.Rectangle;
import hibernate5tut.practice.inheritance.tableperclass.Shape;
import hibernate5tut.practice.interceptor.LogInterceptor;
import hibernate5tut.practice.listener.EventListenerIntegrator;
import hibernate5tut.practice.mapping.onetoone.Account;
import hibernate5tut.practice.mapping.onetoone.AccountDetails;
import hibernate5tut.practice.mapping.onetoone.Authority;
import hibernate5tut.practice.mapping.primary.UserResourceAction;
import hibernate5tut.practice.mapping.recursive.Menu;
import hibernate5tut.practice.query.named.Client;
import hibernate5tut.practice.query.named.Site;
import hibernate5tut.practice.state.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public final class HibernateUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(HibernateUtils.class);

    private HibernateUtils() {
        throw new IllegalStateException("Utility class can not be initialize");
    }

    private static Map<String, SessionFactory> factories = new ConcurrentHashMap<>();

    public static SessionFactory getSessionFactory(DatabaseVendor vendor) {
        LOGGER.debug(">>> Get Session factory for [{}]  with config file [{}]", vendor.name(), vendor.getConfigFile());
        return build(vendor.getConfigFile());
    }

    private static SessionFactory build(String configFileName) {
        SessionFactory factory = factories.get(configFileName);
        if (factory == null) {
            BootstrapServiceRegistry bootstrapRegistry =
                    new BootstrapServiceRegistryBuilder()
                            .applyIntegrator(new EventListenerIntegrator()) // Register event listener
                            .build();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder(bootstrapRegistry)
                    .configure(configFileName) // Load config file from resource folder
                    .build();

            Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            factory = metadata.getSessionFactoryBuilder()
                    .applyInterceptor(new LogInterceptor()) // Register interceptor
                    .build();

            factories.put(configFileName, factory);
        }
        return factory;
    }

    /**
     * build  session factory programmatically
     * @param configFileName
     * @return
     */
    private static SessionFactory buildProgrammatically(String configFileName) {
        SessionFactory factory = factories.get(configFileName);
        if (factory == null) {
            BootstrapServiceRegistry bootstrapRegistry =
                    new BootstrapServiceRegistryBuilder()
                            .applyIntegrator(new EventListenerIntegrator()) // Register event listener
                            .build();
            Properties properties = new Properties();
            properties.put("format_sql", "true"); // override property
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder(bootstrapRegistry)
                    .applySettings(properties)
                    .build();

            Metadata metadata = new MetadataSources(serviceRegistry)
                    .addAnnotatedClass(Clazz.class)
                    .addAnnotatedClass(Student.class)
                    .addAnnotatedClass(Product.class)
                    .addAnnotatedClass(Company.class)
                    .addAnnotatedClass(Person.class)
                    .addAnnotatedClass(Account.class)
                    .addAnnotatedClass(AccountDetails.class)
                    .addAnnotatedClass(Authority.class)
                    .addAnnotatedClass(Menu.class)
                    .addAnnotatedClass(Site.class)
                    .addAnnotatedClass(Client.class)
                    .addAnnotatedClass(UserResourceAction.class)
                    .addAnnotatedClass(AbstractEmployee.class)
                    .addAnnotatedClass(ContractEmployee.class)
                    .addAnnotatedClass(PermanentEmployee.class)
                    .addAnnotatedClass(AbstractUser.class)
                    .addAnnotatedClass(Sender.class)
                    .addAnnotatedClass(Shipper.class)
                    .addAnnotatedClass(Receiver.class)
                    .addAnnotatedClass(Shape.class)
                    .addAnnotatedClass(Circle.class)
                    .addAnnotatedClass(Rectangle.class)
                    .getMetadataBuilder()
                    .build();

            factory = metadata.getSessionFactoryBuilder()
                    .applyInterceptor(new LogInterceptor()) // Register interceptor
                    .build();

            factories.put(configFileName, factory);
        }
        return factory;
    }

    public static <T> T execute(Function<Session, T> consumer, DatabaseVendor vendor) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = null;
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            T entities = consumer.apply(session);
            session.getTransaction().commit();
            return entities;
        } catch (Exception e) {
            session.getTransaction().rollback();
            throw e;
        } finally {
            if (session != null) {
                session.close();
            }
        }

    }

}
