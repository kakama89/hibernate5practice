CREATE TABLE `hibernate_sequences` (
  `sequence_name` varchar(255) NOT NULL,
  `sequence_next_hi_value` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`sequence_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- structure for tbl_clazz
CREATE TABLE `tbl_clazz` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_student
CREATE TABLE `tbl_student` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `clazz_id` bigint(20),
  `resits` int not null default 0,
  CONSTRAINT `FK_tbl_student_tbl_clazz__clazz_id` FOREIGN KEY (`clazz_id`) REFERENCES `tbl_clazz` (`id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_company
CREATE TABLE `tbl_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_product
CREATE TABLE `tbl_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `company_id` bigint(20),
  CONSTRAINT `FK_tbl_product_tbl_company__company_id` FOREIGN KEY (`company_id`) REFERENCES `tbl_company` (`id`),
  CONSTRAINT `UK_tbl_product__code` UNIQUE KEY (`code`) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_person
CREATE TABLE `tbl_person` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- structure for tbl_account
CREATE TABLE `tbl_account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_account_detail
CREATE TABLE `tbl_account_detail` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `avatar_url` varchar(255) NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  CONSTRAINT `FK_tbl_account_detail_tbl_account__account_id` FOREIGN KEY (`account_id`) REFERENCES `tbl_account` (`id`),
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_authority
CREATE TABLE `tbl_authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `UK_tbl_authority__name` UNIQUE KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_account_authority
CREATE TABLE `tbl_account_authority` (
  `account_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  CONSTRAINT `FK_tbl_account_authority_tbl_account__account_id` FOREIGN KEY (`account_id`) REFERENCES `tbl_account` (`id`),
  CONSTRAINT `FK_tbl_account_authority_tbl_authority__authority_id` FOREIGN KEY (`authority_id`) REFERENCES `tbl_authority` (`id`),
  PRIMARY KEY (`account_id`,`authority_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- structure for tbl_menu
CREATE TABLE `tbl_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` bigint(20) NULL,
  `priority` int NOT NULL default 0,
  CONSTRAINT `FK_tbl_menu_tbl_menu__parent_id` FOREIGN KEY (`parent_id`) REFERENCES `tbl_menu` (`id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- structure for tbl_site
CREATE TABLE `tbl_site` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `time_zone` varchar(20) NULL,
  CONSTRAINT `UK_tbl_site__alias` UNIQUE KEY (`alias`) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_site
CREATE TABLE `tbl_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `site_id` bigint(20) NOT NULL,
  CONSTRAINT `UK_tbl_client__alias` UNIQUE KEY (`alias`) ,
  CONSTRAINT `FK_tbl_client_tbl_site__site_id` FOREIGN KEY (`site_id`) REFERENCES `tbl_site` (`id`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_user_resource_action
CREATE TABLE `tbl_user_resource_action`(
  `user_id` bigint(20) NOT NULL,
  `resource_id` bigint(20) NOT NULL,
  `action_id` bigint(20) NOT NULL,
  `description` varchar(100) NULL,
  `status` varchar(20) NULL,
  PRIMARY KEY (`user_id`,`resource_id`,`action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- structure for tbl_employee
CREATE TABLE `tbl_employee`(
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(10) NOT NULL,
  `health_packages` int NULL,
  `end_date` timestamp NULL,
  `created_date` timestamp null,
  `created_by` varchar(255) null,
  `last_modified_date` timestamp null,
  `last_modified_by` varchar(255) null,
  `version` bigint(20) not null default 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_user
CREATE TABLE `tbl_user`(
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_shipper
CREATE TABLE `tbl_shipper`(
  `shipper_id` bigint(20) NOT NULL ,
  `output_limit` boolean NULL,
  PRIMARY KEY (`shipper_id`),
  CONSTRAINT `FK_tbl_shipper_tbl_user__shipper_id` FOREIGN KEY (`shipper_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_sender
CREATE TABLE `tbl_sender`(
  `sender_id` bigint(20) NOT NULL ,
  `card_id` varchar(255) NULL,
  `tax_id` varchar(255) NULL,
  `email` varchar(255) NULL,
  `shipper_id` bigint(20) NULL,
  PRIMARY KEY (`sender_id`),
  CONSTRAINT `FK_tbl_sender_tbl_user__sender_id` FOREIGN KEY (`sender_id`) REFERENCES `tbl_user` (`id`),
  CONSTRAINT `FK_tbl_sender_tbl_shipper__shipper_id` FOREIGN KEY (`shipper_id`) REFERENCES `tbl_shipper` (`shipper_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_receiver
CREATE TABLE `tbl_receiver`(
  `receiver_id` bigint(20) NOT NULL ,
  `province_id` bigint(20) NULL,
  `remote_area_fee` bigint(20) NULL,
  PRIMARY KEY (`receiver_id`),
  CONSTRAINT `FK_tbl_receiver_tbl_user__receiver_id` FOREIGN KEY (`receiver_id`) REFERENCES `tbl_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_shape

CREATE TABLE `tbl_shape` (
  `id` bigint(20) NOT NULL ,
  `shape_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_circle
CREATE TABLE `tbl_circle` (
  `id` bigint(20) NOT NULL ,
  `shape_name` varchar(255) DEFAULT NULL,
  `radius` bigint(20)  NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- structure for tbl_rectangle
CREATE TABLE `tbl_rectangle` (
  `id` bigint(20) NOT NULL ,
  `shape_name` varchar(255) DEFAULT NULL,
  `height` bigint(20) NOT NULL,
  `width` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;