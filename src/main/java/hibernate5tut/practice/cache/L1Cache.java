package hibernate5tut.practice.cache;

import hibernate5tut.practice.state.Person;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class L1Cache {
    private static final Logger LOGGER = LoggerFactory.getLogger(L1Cache.class);

    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.POSTGRES;
//        L1Cache.itOnlyHitDatabase1TimeForMultipleSelectionsOfSameIdentify(vendor, 1L);
//        L1Cache.itOnlyHitDatabase1TimeBecauseTheSecondQueryGotCachedValueFromFirstQuery(vendor, 1L);
//        L1Cache.itOnlyHit1TimeToDatabaseUsingGetReferenceMethod(vendor, 1L);
//        L1Cache.itOnlyHit1TimeToDatabaseUsingGetFindMethod(vendor, 1L);
//
//        L1Cache.hit2TimesForSelectDifferenceIdentifies(vendor, 1L, 2L);
//        L1Cache.notCacheForHQLQuery(vendor, 1L);
//        L1Cache.notCacheForCriteriaQuery(vendor, 1L);
//        L1Cache.queryFromSession2DoesNotGetCachedValueFromSession1(vendor, 1L);
        Person person = L1Cache.load(vendor, 1525L);
         System.err.println(person);
    }

    public static Person get(DatabaseVendor vendor, Long id) {
        Function<Session, Person> func = session -> {
            Person firstGet = session.get(Person.class, id); // load person and save to cache
            return firstGet;
        };
        return HibernateUtils.execute(func, vendor);
    }

    public static Person load(DatabaseVendor vendor, Long id) {
        Function<Session, Person> func = session -> {
            Person firstGet = session.load(Person.class, id); // load person and save to cache
            return firstGet;
        };
        return HibernateUtils.execute(func, vendor);
    }

    public static Person getReference(DatabaseVendor vendor, Long id) {
        Function<Session, Person> func = session -> {
            Person firstGet = session.getReference(Person.class, id); // load person and save to cache
            return firstGet;
        };
        return HibernateUtils.execute(func, vendor);
    }


    public static void itOnlyHitDatabase1TimeForMultipleSelectionsOfSameIdentify(DatabaseVendor vendor, Long id) {
        Function<Session, Void> func = session -> {
            Person firstGet = session.get(Person.class, id); // load person and save to cache
            Person secondGet = session.load(Person.class, id);  // found from cache , it won't hit to database
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void itOnlyHitDatabase1TimeBecauseTheSecondQueryGotCachedValueFromFirstQuery(DatabaseVendor vendor, Long id) {
        Function<Session, List<Person>> func = session -> {
            List<Person> allPersons = session.createQuery("From Person").list(); // load persons and save to persistent context
            Person secondGet = session.get(Person.class, id); // found from cache , it won't hit to database
            return allPersons;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void hit2TimesForSelectDifferenceIdentifies(DatabaseVendor vendor, Long firstId, Long secondId) {
        Function<Session, Void> func = session -> {

            Person firstGet = session.get(Person.class, firstId); // get person with id firstId from database and save to cache
            LOGGER.error("First person {}", firstGet);

            // not found person with id secondId from cache so it will hit to database to retrieve person with id =secondId
            Person secondGet = session.load(Person.class, secondId);

            LOGGER.error("Second person {}", secondGet); // force to call proxy

            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void notCacheForHQLQuery(DatabaseVendor vendor, Long id) {
        Function<Session, Void> func = session -> {
            Person firstGet = session.get(Person.class, id); // get person with id id from database and save to cache

            // this query does not try to get from cache
            Person secondGet = session.createQuery("from Person where id = :id", Person.class)
                    .setParameter("id", id)
                    .uniqueResult();
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void notCacheForCriteriaQuery(DatabaseVendor vendor, Long id) {
        Function<Session, Void> func = session -> {
            Person firstGet = session.get(Person.class, id);
            // this query does not try to get from cache
            Criteria criteria = session.createCriteria(Person.class);
            criteria.add(Restrictions.eq("id", id));
            Person secondGet = (Person) criteria.uniqueResult();
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void itOnlyHit1TimeToDatabaseUsingGetReferenceMethod(DatabaseVendor vendor, Long id) {
        Function<Session, Void> func = session -> {
            Person firstGet = session.get(Person.class, id);
            // not found person with id secondId from cache so it will hit to database to retrieve person with id =secondId
            // this query does not try to get from cache
            Person secondGet = session.getReference(Person.class, id);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void itOnlyHit1TimeToDatabaseUsingGetFindMethod(DatabaseVendor vendor, Long id) {
        Function<Session, Void> func = session -> {
            Person firstGet = session.get(Person.class, id);
            // not found person with id secondId from cache so it will hit to database to retrieve person with id =secondId
            // this query does not try to get from cache
            Person secondGet = session.find(Person.class, id);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void queryFromSession2DoesNotGetCachedValueFromSession1(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session firstSession = factory.openSession(); // session 1 have its persistent context
        Transaction tx = firstSession.beginTransaction();
        Person firstPerson = firstSession.get(Person.class, id);
        tx.commit();
        firstSession.close();

        Session secondSession = factory.openSession();
        Transaction tx2 = secondSession.beginTransaction();
        // query from session 2 does not get cached value from session 1.Therefor this query will hit to database
        Person person = secondSession.get(Person.class, id);
        tx2.commit();
        secondSession.close();
    }
}
