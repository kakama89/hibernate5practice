package hibernate5tut.practice.mapping.onetoone;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_account_id_seq_generator")
    @SequenceGenerator(name = "tbl_account_id_seq_generator", sequenceName = "tbl_account_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "username")
    private String username;

    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    private AccountDetails accountDetail;

    /**
     * can map with one to many
     */
//    @OneToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "tbl_account_authority",
//            joinColumns = {
//                    @JoinColumn(name = "account_id")
//            },
//            inverseJoinColumns = {
//                    @JoinColumn(name = "authority_id")
//            })
//

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tbl_account_authority",
            joinColumns = {
                    @JoinColumn(name = "account_id", referencedColumnName = "id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "authority_id", referencedColumnName = "id")
            })
    private List<Authority> authorities;
}
