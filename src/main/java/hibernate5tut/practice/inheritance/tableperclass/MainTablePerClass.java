package hibernate5tut.practice.inheritance.tableperclass;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;

import java.util.List;
import java.util.function.Function;

public class MainTablePerClass {
    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;

        MainTablePerClass.createShape(vendor);
        MainTablePerClass.createCircle(vendor);
        MainTablePerClass.createRectangle(vendor);

        MainTablePerClass.getShapes(vendor);
        MainTablePerClass.getCircle(vendor);
    }

    public static void createCircle(DatabaseVendor vendor) {
        Function<Session, Void> func = session -> {
            Circle circle = new Circle();
            circle.setShapeName("Circle " + (int) (Math.random() * 100));
            circle.setRadius(50L);
            session.persist(circle);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void createRectangle(DatabaseVendor vendor) {
        Function<Session, Void> func = session -> {
            Rectangle rectangle = new Rectangle();
            rectangle.setShapeName("Rectang " + (int) (Math.random() * 100));
            rectangle.setWidth(50L);
            rectangle.setHeight(100L);
            session.persist(rectangle);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void createShape(DatabaseVendor vendor) {
        Function<Session, Void> func = session -> {
            Shape shape = new Shape();
            shape.setShapeName("Shape " + (int) (Math.random() * 100));
            session.persist(shape);
            return (Void) null;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void getShapes(DatabaseVendor vendor) {
        Function<Session, List<Shape>> func = session -> {
            List<Shape> shapes = session.createQuery("From Shape", Shape.class).list();
            return shapes;
        };
        HibernateUtils.execute(func, vendor);
    }

    public static void getCircle(DatabaseVendor vendor) {
        Function<Session, List<Circle>> func = session -> {
            List<Circle> circles = session.createQuery("From Circle", Circle.class).list();
            return circles;
        };
        HibernateUtils.execute(func, vendor);
    }
}
