package hibernate5tut.practice.query.hql;

import hibernate5tut.practice.state.Person;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Tuple;
import java.util.List;
import java.util.function.Function;

public class MainHQL {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainHQL.class);

    public static void main(String[] args) {
        MainHQL.getAllClassAggregation(DatabaseVendor.MYSQL);
    }


    /**
     * Get all company group by company name, count number of products for each company
     *
     * @param vendor
     */
    public static void getAllCompany(DatabaseVendor vendor) {
        Function<Session, List<Tuple>> func = session -> session.createQuery("select c.name , count(p) From Company c join c.products p group by c.name", Tuple.class).list();
        List<Tuple> result = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>> Company : {}", result);
    }

    public static void getAllClassAggregation(DatabaseVendor vendor) {
        Function<Session, List<ClassAggDTO>> func = session -> session.createQuery(
                " select new hibernate5tut.practice.query.hql.ClassAggDTO(c.name, count(s),avg(s.resits) , sum(s.resits), max(s.resits))" +
                        " from Clazz c join c.students s " +
                        " group by c.name " +
                        " having  avg(s.resits) >= 1 ", ClassAggDTO.class).list();
        List<ClassAggDTO> classes = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>> Classes : {}", classes);
    }


    public static void selectAllPerson(DatabaseVendor vendor) {
        Function<Session, List<Person>> func = session -> session.createQuery("From Person p", Person.class).list();
        List<Person> persons = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>> Persons : {}", persons);
    }


    public static void updatePerson(DatabaseVendor vendor, Long id, String newName) {
        Function<Session, Integer> func = session -> {
            int updatedRows = session
                    .createQuery("UPDATE Person p SET p.name = :newName where p.id = :id ")
                    .setParameter("newName", newName)
                    .setParameter("id", id)
                    .executeUpdate();
            return updatedRows;

        };
        HibernateUtils.execute(func, vendor);
    }

    public static void delete(DatabaseVendor vendor, Long id) {
        Function<Session, Integer> func = session -> {
            int updatedRows = session
                    .createQuery("DELETE FROM Person p where p.id = :id ")
                    .setParameter("id", id)
                    .executeUpdate();
            return updatedRows;

        };
        HibernateUtils.execute(func, vendor);
    }


}
