package hibernate5tut.practice.fetch.lazy;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;

import java.util.List;
import java.util.Set;
import java.util.function.Function;

/**
 * This problem occurs with LAZY load
 * When load root entity hibernate only load association as soon as first access association
 * Assume we have load 100 entities and for each entity we have 1 association collection with 10 other entities
 * And in for loop we try to get association collection -> we get n +1 query
 * List<Clazz> roots = session.get(Clazz.class);
 * for(int i = 0 ; i < roots.size() ; i++){
 * Clazz root = roots.get(i);
 * List<Students> students = root.getStudents(); // get association for each iteration
 * System.out.println(students);
 * }
 * <p>
 * <p>
 * 1 query for selectAllCompany 100 root entities.
 * for each in 100 iteration hibernate try get association by 1 query
 * so we have 1 + 100 = 101 query
 * <p>
 * <p>
 * TO reduce number of sql query use can use
 * 1. mask @BatchSize annotation in entity (reduce) {@link Clazz#students}
 * 2. user join fetch in hql {@link MainLazy#getAssociationSessionCloseOkUseHQLJoinFetch}
 * 3. use fetch criteria query {@link MainLazy#getAssociationSessionCloseOkUseCriteriaBuilderWithFetch}
 * 4. use fetch mode with hibernate criteria {@link MainLazy#getAssociationSessionCloseOkUseCriteriaJoinFetch}
 * 5. use entity graph {@link MainLazy#getAssociationSessionCloseOkUseEntityGraph}
 */
public class Nplus1QueryProblem {
    public static void main(String[] args) {
        Nplus1QueryProblem.reproduceNplus1Problem(DatabaseVendor.POSTGRES);
    }

    public static void reproduceNplus1Problem(DatabaseVendor vendor) {
        Function<Session, List<Clazz>> func = session -> {
            List<Clazz> classes = session.createQuery("From Clazz", Clazz.class).list();
            for (Clazz clazz : classes) {
                Set<Student> students = clazz.getStudents();
                students.size(); // force to access association
            }
            Clazz c = new Clazz();
            session.save(c);
            return classes;
        };
        List<Clazz> classes = HibernateUtils.execute(func, vendor);
    }
}
