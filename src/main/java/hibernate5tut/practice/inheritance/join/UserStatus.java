package hibernate5tut.practice.inheritance.join;

public enum UserStatus {
    ACTIVE, INACTIVE, LOCK;

    public boolean isNotEquals(UserStatus other) {
        return !equals(other);
    }
}