SET SCHEMA 'public';
-- insert data for table tbl_clazz
INSERT INTO tbl_clazz(id,name) VALUES
(1,'Class 1'),
(2,'Class 2'),
(3,'Class 3'),
(4,'Class 4'),
(5,'Class 5'),
(6,'Class 6'),
(7,'Class 7'),
(8,'Class 8'),
(9,'Class 9'),
(10,'Class 10');
SELECT setval('tbl_clazz_id_seq', 10, true);

-- insert data for table tbl_student
INSERT INTO tbl_student(id, name, clazz_id, resits)VALUES
    (1,'A1',1,2),    (2,'B1',1,0),     (3,'C1',1,0),     (4,'D1',1,1),     (5,'E1',1,3),
    (6,'A2',2,1),     (7,'B2',2,0),     (8,'C2',2,0),     (9,'D2',2,1),     (10,'E2',2,1),
    (11,'A3',3,1),    (12,'B3',3,2),    (13,'C3',3,0),    (14,'D3',3,1),    (15,'E3',3,1),
    (16,'A4',4,0),    (17,'B4',4,0),    (18,'C4',4,0),    (19,'D4',4,2),    (20,'E4',4,0),
    (21,'A6',5,3),    (22,'B5',5,1),    (23,'C5',5,0),    (24,'D5',5,2),    (25,'E5',5,10),
    (26,'A6',6,2),    (27,'B6',6,3),    (28,'C6',6,0),    (29,'D6',6,2),    (30,'E6',6,1),
    (31,'A7',7,1),    (32,'B7',7,0),    (33,'C7',7,0),    (34,'D7',7,2),    (35,'E7',7,2),
    (36,'A8',8,5),    (37,'B8',8,0),    (38,'C8',8,0),    (39,'D8',8,0),    (40,'E8',8,1),
    (41,'A9',9,7),    (42,'B9',9,0),    (43,'C9',9,1),    (44,'D9',9,0),    (45,'E9',9,0),
    (46,'A10',10,4),  (47,'B10',10,0),  (48,'C10',10,0),  (49,'D10',10,0),  (50,'E10',10,0);
SELECT setval('tbl_student_id_seq', 50, true);

-- insert data for table tbl_company
INSERT INTO tbl_company(id,name) VALUES
  (1,'NashTech'),
  (2,'Fpt Software');

SELECT setval('tbl_company_id_seq', 2, true);

-- insert data for table tbl_product
INSERT INTO tbl_product(id,code,company_id) VALUES
  (1,'NT001',1),
  (2,'NT002',1),
  (3,'NT003',1),
  (4,'FPT001',2),
  (5,'FPT002',2);
SELECT setval('tbl_product_id_seq', 5, true);


-- insert data for table tbl_person
INSERT INTO tbl_person(id,name) VALUES
(1,'Nguyen Dang Khoa'),
(2,'Nguyen Trung Kien');
SELECT setval('tbl_person_id_seq', 2, true);

-- insert data for table tbl_account
INSERT INTO tbl_account(id,username) VALUES
(1,'khoahihi@gmail.com'),
(2,'kienntbn@gmail.com');
SELECT setval('tbl_account_id_seq', 2, true);

-- insert data for table tbl_account_detail
INSERT INTO tbl_account_detail(account_id,avatar_url,phone_number) VALUES
(1,'http://avatar.com/khoa.png','0123456789'),
(2,'http://avatar.com/kien.png','0987654321');

-- insert data for table tbl_authority
INSERT INTO tbl_authority(id,name) VALUES
(1, 'Shipper'),
(2, 'Receiver'),
(3, 'Sender');
SELECT setval('tbl_authority_id_seq', 3, true);

-- insert data for table tbl_account_authority
INSERT INTO tbl_account_authority(account_id,authority_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(2,1),
(2,3);
-- insert data for table tbl_menu
INSERT INTO tbl_menu(id,name,parent_id,priority) VALUES
  (1,'Xa Hoi',null,1),
  (2,'Chinh Tri',1,1),
  (3,'Moi Truong',1,2),
  (4,'Giao Thong',1,3),
  (5,'The Gioi',null,2),
  (6,'Chau A',5,1),
  (7,'Eu & My',5,2),
  (8,'Chau My',5,3),
  (9,'Diem Nong',5,4);
SELECT setval('tbl_menu_id_seq', 9, true);


-- insert data for table tbl_site
INSERT INTO tbl_site(id,alias,name,time_zone) VALUES
  (1,'S001','FPT Building','Asia/Bangkok'),
  (2,'S002','HITC Building','Australia/Tasmania'),
  (3,'S003','Landmark 72','Canada/Pacific'),
  (4,'S004','Vincom Plaza','Europe/Luxembourg'),
  (5,'S005','PVI tower','America/New_York');
SELECT setval('tbl_site_id_seq', 5, true);

-- insert data for table tbl_client
INSERT INTO tbl_client(id,alias,name,site_id) VALUES
(1,'FSOFT','FPT Software',1),
(2,'VNEXP','VN Express',1),
(3,'NASH','Nash Tech',2),
(4,'VAMED','Vamed Healthcare',2),
(5,'BAYER','Bayer VN',2),
(6,'LG','LG VN',3),
(7,'Vin','Vin',4),
(8,'SAMSUNG','Samsung Viet Nam',5);
SELECT setval('tbl_client_id_seq', 8, true);

-- insert data for table tbl_user_resource_action
INSERT INTO tbl_user_resource_action(user_id,resource_id,action_id, description,status) VALUES
(1,2,3,'1L_2L_3L','ACTIVE'),
(4,5,6,'4L_5L_6L','INACTIVE');

-- insert data for table tbl_employee
INSERT INTO tbl_employee(id, name, type, health_packages, end_date) VALUES
(1, 'Tung','CONTRACT', null, now()),
(2, 'TOAI','PERMANENT', 2, null);
SELECT setval('tbl_employee_id_seq', 2, true);


-- insert data for table tbl_user
INSERT INTO tbl_user(id,name,status) VALUES
(1, 'Kien','ACTIVE'),
(2, 'Khoa','ACTIVE'),
(3, 'Khoa','ACTIVE');
SELECT setval('tbl_user_id_seq', 2, true);


-- insert data for table tbl_shipper
INSERT INTO tbl_shipper(shipper_id,output_limit) VALUES
(1, true);

-- insert data for table tbl_sender
INSERT INTO tbl_sender(sender_id,card_id,tax_id,email,shipper_id) VALUES
(2, '142484241','0032581001','khoahihi@gmail.com',1);

-- insert data for table tbl_sender
INSERT INTO tbl_receiver(receiver_id,province_id,remote_area_fee) VALUES
(3, 30,150);