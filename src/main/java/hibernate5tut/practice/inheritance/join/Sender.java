package hibernate5tut.practice.inheritance.join;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_sender")
@PrimaryKeyJoinColumn(name = "sender_id")
public class Sender extends AbstractUser {

    @Column(name = "card_id")
    private String cardId;

    @Column(name = "tax_id")
    private String taxId;

    @Column(name = "email")
    private String email;

    @ManyToOne(targetEntity = Shipper.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "shipper_id")
    private AbstractUser shipper;

    @Override
    public String toString() {
        return MessageFormat.format("Sender[id {0}, name {1}, status {2}, cardId {3}, taxId {4}, email {5}]",
                getId(), getName(), getStatus(), cardId, taxId, email);
    }
}
