package hibernate5tut.practice.query.paging;

import hibernate5tut.practice.fetch.lazy.Student;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.function.Function;

/**
 * user setFirstResult and setMaxResults in hibernate query to pagination
 */
public class MainPaging {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainPaging.class);

    public static void main(String[] args) {
        MainPaging.getStudentsCriteriaBuilder(DatabaseVendor.POSTGRES, 0, 10);
//        MainPaging.getStudentsHQL(DatabaseVendor.POSTGRES, 10, 10);
    }

    public static List<Student> getStudentsCriteriaBuilder(DatabaseVendor vendor, int offset, int limit) {
        Function<Session, List<Student>> func = session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Student> criteriaQuery = builder.createQuery(Student.class);
            Root<Student> root = criteriaQuery.from(Student.class);
            criteriaQuery.select(root);
            return session.createQuery(criteriaQuery)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .list();
        };
        List<Student> students = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> offset {}, limit {}, students  : {}", offset, limit, students);
        return students;
    }

    public static List<Student> getStudentsHQL(DatabaseVendor vendor, int offset, int limit) {
        Function<Session, List<Student>> func = session -> {
            List<Student> students = session.createQuery("From Student", Student.class)
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .list();
            return students;
        };
        List<Student> students = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> offset {}, limit {}, students  : {}", offset, limit, students);
        return students;
    }
}
