package hibernate5tut.practice.inheritance.singletable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.text.MessageFormat;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@Entity
@DiscriminatorValue(value = "CONTRACT")
public class ContractEmployee extends AbstractEmployee {

    @Column(name = "end_date")
    private Instant endDate;

    @Override
    public String toString() {
        return MessageFormat.format("ContractEmployee[id {0}, name {1}, endDate {2} ]", getId(), getName(), endDate);
    }
}
