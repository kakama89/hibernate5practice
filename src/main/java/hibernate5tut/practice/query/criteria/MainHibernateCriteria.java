package hibernate5tut.practice.query.criteria;

import hibernate5tut.practice.fetch.lazy.Student;
import hibernate5tut.practice.fetch.lazy.Student_;
import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.function.Function;

public class MainHibernateCriteria {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainHibernateCriteria.class);

    public static void main(String[] args) {
        MainHibernateCriteria.withGroupByAndHaving(DatabaseVendor.MYSQL);
    }

    /**
     * Select students which id <20 or id > 40
     *
     * @param vendor
     */
    public static void withSimpleCriteria(DatabaseVendor vendor) {
        Function<Session, List<Long>> func = session -> {
            Criteria criteria = session.createCriteria(Student.class);
            criteria.setProjection(Projections.property(Student_.ID));
            criteria.add(
                    Restrictions.or(
                            Restrictions.lt(Student_.ID, 20L),
                            Restrictions.gt(Student_.ID, 40L)
                    )
            );
            return criteria.list();
        };
        List<Long> students = HibernateUtils.execute(func, vendor);
        LOGGER.error(">>>> list students id  : {}", students);
    }

    /**
     * Projection is the case when you want to
     * selectAllCompany one or multiple column
     * in this case we want to selectAllCompany id, name of  classes
     * which name start with A
     */
    public static void withProjection(DatabaseVendor vendor) {
        Function<Session, List<Object>> func = session -> {
            Criteria criteria = session.createCriteria(Student.class);
            criteria.setProjection(
                    Projections.projectionList()
                            .add(Projections.property(Student_.ID))
                            .add(Projections.property(Student_.NAME).as("studentName")) // can use with alias
            );
            criteria.add(Restrictions.like(Student_.NAME, "A%"));
            return criteria.list();
        };
        List<Object> students = HibernateUtils.execute(func, vendor);
        for (Object student : students) {
            LOGGER.error(">>>> student  : {}", student);
        }

    }

    public static void withGroupBy(DatabaseVendor vendor) {
        Function<Session, List<Object>> func = session -> {
            Criteria criteria = session.createCriteria(Student.class, "s");
            criteria.createAlias("s.clazz", "c");
            criteria.setProjection(
                    Projections.projectionList()
                            .add(Projections.property("c.id"))
                            .add(Projections.groupProperty("c.id"))
                            .add(Projections.rowCount())
                            .add(Projections.avg(Student_.RESITS))
            );
//            criteria.add(Restrictions.like(Student_.NAME, "A%"));
            return criteria.list();
        };
        List<Object> students = HibernateUtils.execute(func, vendor);
        for (Object student : students) {
            LOGGER.error(">>>> student  : {}", student);
        }
    }

    public static void withGroupByAndHaving(DatabaseVendor vendor) {
        Function<Session, List<Object>> func = session -> {
            Criteria criteria = session.createCriteria(Student.class);
            criteria.setProjection(
                    Projections.projectionList()
                            .add(Projections.sqlGroupProjection(
                                    "{alias}.clazz_id as clazzId, {alias}.resits as avgResits", // sql query
                                    "clazzId , avgResits having avgResits > 0", // group by clause
                                    new String[]{"clazzId", "avgResits"}, // list alias
                                    new Type[]{LongType.INSTANCE, DoubleType.INSTANCE})
                            )

            );
            criteria.add(Restrictions.like(Student_.NAME, "A%"));
            return criteria.list();
        };
        List<Object> students = HibernateUtils.execute(func, vendor);
        for (Object student : students) {
            LOGGER.error(">>>> student  : {}", student);
        }
    }
}
