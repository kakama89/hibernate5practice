package jdbc.dao.dao;

import hibernate5tut.practice.inheritance.join.Sender;
import hibernate5tut.practice.state.Person;

import java.sql.SQLException;
import java.util.List;

public interface SenderDao {
    List<Sender> getAllSender() throws SQLException;
}
