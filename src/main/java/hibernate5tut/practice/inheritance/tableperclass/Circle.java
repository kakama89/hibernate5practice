package hibernate5tut.practice.inheritance.tableperclass;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "tbl_circle")
public class Circle extends Shape {
    @Column(name = "radius")
    private Long radius;
}
