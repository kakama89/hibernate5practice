package hibernate5tut.practice.mapping.primary;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_user_resource_action")
public class UserResourceAction {
    public enum Status {
        ACTIVE, INACTIVE
    }

    public UserResourceAction(UserResourceActionId id) {
        this.id = id;
    }

    @EmbeddedId
    private UserResourceActionId id;

    @Column(name = "description")
    private String description;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Override
    public String toString() {
        return MessageFormat.format("\nUserResourceAction[id {0}, description {1}, status {2}]", id, description, status);
    }
}
