package hibernate5tut.practice.mapping.recursive;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.text.MessageFormat;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "tbl_menu")
@Cacheable
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_menu_id_seq_generator")
    @SequenceGenerator(name = "tbl_menu_id_seq_generator", sequenceName = "tbl_menu_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "priority")
    private Integer priority;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Menu parent;

    @OrderBy(value = "priority ASC")
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<Menu> children;

    public String toString() {
        return MessageFormat.format("\nMenu[id {0}, name {1}, priority {2}]", id, name, priority);
    }
}
