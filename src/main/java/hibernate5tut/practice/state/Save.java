package hibernate5tut.practice.state;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class Save {
    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;
//        Save.saveDetach(vendor);
        Save.saveDetach2(vendor);
    }

    public static void saveTransient(DatabaseVendor vendor) { // will insert new record
        Person transientPerson = new Person();
        transientPerson.setName("Do Xuan Toai");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(transientPerson);
        tx.commit();
        session.close();
    }

    public static void saveDetach(DatabaseVendor vendor) { // will insert new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        session.evict(persistence);
        Person detach = persistence;
        detach.setName("Save a detach object");
        session.save(detach);
        tx.commit();
        session.close();
    }

    public static void saveDetach2(DatabaseVendor vendor) { // will insert new record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        Person detach = new Person();
        detach.setId(1000L);
        detach.setName("Save a detach object 2222");
        session.save(detach);
        tx.commit();
        session.close();
    }


    public static void savePersistence(DatabaseVendor vendor) { // will update persistence record
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistence = session.find(Person.class, 1L);
        persistence.setName("Save a persistence object");
        session.save(persistence);
        tx.commit();
        session.close();
    }
}
