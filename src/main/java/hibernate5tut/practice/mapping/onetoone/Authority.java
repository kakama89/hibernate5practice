package hibernate5tut.practice.mapping.onetoone;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tbl_authority")
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_authority_id_seq_generator")
    @SequenceGenerator(name = "tbl_authority_id_seq_generator", sequenceName = "tbl_authority_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name", unique = true)
    private String name;

    //    @OneToMany(mappedBy = "authorities", fetch = FetchType.LAZY)
    @ManyToMany(mappedBy = "authorities", fetch = FetchType.LAZY)
    private List<Account> accounts;
}
