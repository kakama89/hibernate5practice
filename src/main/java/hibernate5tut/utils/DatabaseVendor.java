package hibernate5tut.utils;

public enum DatabaseVendor {

    MYSQL("mysql.cfg.xml"), POSTGRES("postgres.cfg.xml");
    private String configFile;

    DatabaseVendor(String configFile) {
        this.configFile = configFile;
    }

    public String getConfigFile() {
        return configFile;
    }
}
