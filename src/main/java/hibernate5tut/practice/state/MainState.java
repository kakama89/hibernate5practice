package hibernate5tut.practice.state;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.*;

/**
 * Transient object :
 * Java object does not refer to database record
 * Persistent object: object is in persistent context
 * Object was loaded from database
 * After call session.save on transient object
 * Detached object is attached by session.lock
 * <p>
 * Detached object:
 * After session.close is call all persistent objects will be in detached state
 * Session is still open but objects were cleared out the context by call session.clear or session.evict
 */

public class MainState {
    public static void main(String[] args) {

        MainState.addPerson(DatabaseVendor.MYSQL);
//        MainState.weCanDeleteDetachEntity(DatabaseVendor.MYSQL, 1L);
//        MainState.updateObjectHaveAlreadyBeenInPersistentContext(DatabaseVendor.MYSQL, 1L);
//        MainState.attachDetachObjectByLock(DatabaseVendor.MYSQL, 1L);
//        MainState.pitFallUseMergeOnDetachEntity(DatabaseVendor.MYSQL, 2L);
//        MainState.stateLessSessionDoesNotUpdateEntityBecauseItDoesNotHaveStateFullPersistenceContext(DatabaseVendor.MYSQL, 1L);
    }

    public static void addPerson(DatabaseVendor vendor) {
        Person transientPerson = new Person();
        transientPerson.setName("Nguyen Thanh Tung");
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(transientPerson); // session.save will add transient object to persistent context
        tx.commit(); // tx.commit will apply the change to database
        session.close();

    }

    public static void attachDetachObjectByLock(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person person = session.load(Person.class, id);
        tx.commit();
        session.close(); // after session close the person object will be in detach state

        Session session2 = factory.openSession();
        Transaction tx2 = session2.beginTransaction();
        session2.lock(person, LockMode.NONE);
        person.setName("We can attach the detached object by using lock");
        session2.merge(person);
        tx2.commit();
        session.close();

    }

    public static void weCannotAttachTransientObjectUsingLock(DatabaseVendor vendor) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Person transientPerson = new Person();
        transientPerson.setName("Attach the transient object");
        Session session = factory.openSession();
        Transaction tx2 = session.beginTransaction();
        session.lock(transientPerson, LockMode.NONE);
        tx2.commit();
        session.close();
    }


    public static void updateObjectHaveAlreadyBeenInPersistentContext(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        Person persistentPerson = session.get(Person.class, id);
        persistentPerson.setName("Update object have already been in persistent context");
        /**
         *  when object is in persistent state
         *  we doesn't have to call session.save or session.merge to save it to database
         *  it will be save to database after call session.flush or tx.commit which will call session.flush
         */
        tx.commit(); // only need to call tx.commit or session.flush();

        session.close();
    }


    public static void pitFallUseMergeOnDetachEntity(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);

        Session session1 = factory.openSession();
        Transaction tx = session1.beginTransaction();
        Person persistentPerson = session1.get(Person.class, id);
        persistentPerson.setName("Donald Trump");
        session1.save(persistentPerson);
        tx.commit();
        session1.close();

        // now we try to change detached entity and merge in another session
        Session session2 = factory.openSession();
        Transaction tx2 = session2.beginTransaction();
        persistentPerson.setName("Joe Biden");
        Person mrX = (Person) session2.merge(persistentPerson);
        persistentPerson.setName("Nguyen Tan Dung"); // new change does not save .
        /**
         * we must call mrX.setName("Nguyen Tan Dung") to apply new change
         * because when call merge hibernate will try to get from persistent context first
         * if found record -> return this record and all new change will be affect
         * if not found -> try to get from database and return new object
         *    -> new change in old object does not affect
         *    -> new change in new object does affect
         * in our case hibernate cannot find object in persistent context of session 2
         * therefor it will find from database and return new object mrX
         */
        tx2.commit();
        session2.close();
    }

    public static void weCanDeleteDetachEntity(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        Session session = factory.openSession();
        session.getTransaction().begin();

        Person person = session.get(Person.class, id);
        session.getTransaction().commit();
        session.close();

        Session session2 = factory.openSession();
        session2.getTransaction().begin();
        session2.delete(person); // delete detach entity
        session2.getTransaction().commit();
    }

    public static void stateLessSessionDoesNotUpdateEntityBecauseItDoesNotHaveStateFullPersistenceContext(DatabaseVendor vendor, Long id) {
        SessionFactory factory = HibernateUtils.getSessionFactory(vendor);
        StatelessSession stateLessSession = factory.openStatelessSession();
        stateLessSession.getTransaction().begin();
        Person person = (Person) stateLessSession.get(Person.class, id);
        person.setName("Name won't be changed");
//        stateLessSession.update(person); // we have to call to update the name
        stateLessSession.getTransaction().commit();
        stateLessSession.close();
    }
}
