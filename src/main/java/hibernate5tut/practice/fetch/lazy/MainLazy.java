package hibernate5tut.practice.fetch.lazy;

import hibernate5tut.utils.DatabaseVendor;
import hibernate5tut.utils.HibernateUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.graph.GraphSemantic;
import org.hibernate.graph.RootGraph;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Set;
import java.util.function.Function;

public class MainLazy {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainLazy.class);

    public static void main(String[] args) {
        DatabaseVendor vendor = DatabaseVendor.MYSQL;
        MainLazy.getAssociationSessionCloseOkUseDynamicEntityGraph(vendor, 1L);
    }

    /**
     * We can fetch association of entity that mask FetchType.LAZY
     * when session is still opening
     */
    public static void getAssociationSessionOpenOk(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            Clazz clazz = session.get(Clazz.class, pk);
            MainLazy.printOut(clazz);
            return clazz;
        };
        HibernateUtils.execute(func, vendor);
    }

    /**
     * This method must throw LazyInitializationException
     * Because session has already closed
     * Hibernate cannot get associate entities via proxy
     */
    public static void getAssociationSessionCloseShouldThrowLazyInitializationException(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> session.get(Clazz.class, pk);
        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    /**
     * This method must not throw LazyInitializationException
     * Because Hibernate.initialize will try to load
     * association entities via proxy.
     * We must call Hibernate.initialize(associationEntities)
     * before session is closed
     */
    public static void getAssociationSessionCloseOk01(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            Clazz clazz = session.get(Clazz.class, pk);
            Hibernate.initialize(clazz.getStudents());
            return clazz;
        };
        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    /**
     * This method must not throw LazyInitializationException
     * Because root.fetch("students");
     * will fetch association entities before session is closed
     * So After session is closed , we can get association
     */
    public static void getAssociationSessionCloseOkUseCriteriaBuilderWithFetch(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Clazz> criteria = builder.createQuery(Clazz.class);
            Root<Clazz> root = criteria.from(Clazz.class);
            root.fetch(Clazz_.students); // due to this line
            Predicate whereClause = builder.equal(root.get(Clazz_.id), pk);
            criteria.select(root).where(whereClause);
            Clazz clazz = session.createQuery(criteria)
                    .list()
                    .get(0);
            return clazz;
        };
        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    /**
     * This method must not throw LazyInitializationException
     * Because JOIN FETCH c.students;
     * will fetch association entities before session is closed
     * So After session is closed , we can get association
     */

    public static void getAssociationSessionCloseOkUseHQLJoinFetch(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            Query query = session.createQuery("FROM Clazz c JOIN FETCH c.students WHERE c.id = :id", Clazz.class);
            Clazz clazz = (Clazz) query
                    .setParameter(Clazz_.id.getName(), pk)
                    .list()
                    .get(0);
            return clazz;
        };
        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    /**
     * This method must not throw LazyInitializationException
     * Because criteria.setFetchMode(Clazz_.STUDENTS, FetchMode.JOIN);
     * will fetch association entities before session is closed
     * So After session is closed , we can get association
     */

    public static void getAssociationSessionCloseOkUseCriteriaJoinFetch(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            Criteria criteria = session.createCriteria(Clazz.class);
            criteria.setFetchMode(Clazz_.STUDENTS, FetchMode.JOIN);
            criteria.add(Restrictions.eq(Clazz_.ID, pk));
            Clazz clazz = (Clazz) criteria.list().get(0);
            return clazz;
        };
        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }

    /**
     * This method must not throw LazyInitializationException
     * Because load entity graph;
     * will fetch association entities before session is closed
     * So After session is closed , we can get association
     */

    public static void getAssociationSessionCloseOkUseEntityGraph(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            RootGraph<?> graph = session.createEntityGraph("Clazz.students");
            Query query = session.createQuery("FROM Clazz c WHERE c.id = :id", Clazz.class);
            Clazz clazz = (Clazz) query
                    .setParameter(Clazz_.id.getName(), pk)
                    .setHint(GraphSemantic.LOAD.getJpaHintName(), graph)
                    .list()
                    .get(0);
            return clazz;
        };

        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    /**
     * This method must not throw LazyInitializationException
     * Because load entity graph;
     * will fetch association entities before session is closed
     * So After session is closed , we can get association
     */
    public static void getAssociationSessionCloseOkUseDynamicEntityGraph(DatabaseVendor vendor, Long pk) {
        Function<Session, Clazz> func = session -> {
            RootGraph<?> graph = session.createEntityGraph(Clazz.class);
            graph.addSubgraph(Clazz_.STUDENTS);
            Query query = session.createQuery("FROM Clazz c WHERE c.id = :id", Clazz.class);
            Clazz clazz = (Clazz) query
                    .setParameter(Clazz_.id.getName(), pk)
                    .setHint(GraphSemantic.LOAD.getJpaHintName(), graph)
                    .list()
                    .get(0);
            return clazz;
        };

        Clazz clazz = HibernateUtils.execute(func, vendor);
        MainLazy.printOut(clazz);
    }


    private static void printOut(Clazz clazz) {
        Set<Student> students = clazz.getStudents();
        LOGGER.debug(">>>>>>> Clazz {}, students : {}", clazz.getName(), students);
    }
}
