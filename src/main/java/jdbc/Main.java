package jdbc;

import hibernate5tut.practice.inheritance.join.Sender;
import hibernate5tut.utils.DatabaseVendor;
import jdbc.dao.dao.SenderDao;
import jdbc.dao.impl.SenderDaoImpl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        DatabaseAccessor dbAccessor = new DatabaseAccessor();
        Connection connection = null;
        try {
            connection = dbAccessor.getConnection(DatabaseVendor.MYSQL);
            SenderDao senderDao = new SenderDaoImpl(connection);
            List<Sender> senders = senderDao.getAllSender();
            System.out.println(senders);
        } catch (SQLException e) {
            System.out.println("Cannot get list senders");
            e.printStackTrace();
        } finally {
            dbAccessor.release(connection);
        }
    }
}
