package hibernate5tut.practice.inheritance.join;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_receiver")
@PrimaryKeyJoinColumn(name = "receiver_id")
public class Receiver extends AbstractUser {

    @Column(name = "province_id")
    private Long province;

    @Column(name = "remote_area_fee")
    private Long remoteAreaFee;

    @Override
    public String toString() {
        return MessageFormat.format("Receiver[id {0}, name {1}, status {2}, province {3}, remoteAreaFee {4}]",
                getId(), getName(), getStatus(), province, remoteAreaFee);
    }
}
