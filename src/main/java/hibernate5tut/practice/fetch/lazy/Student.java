package hibernate5tut.practice.fetch.lazy;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.MessageFormat;

@Getter
@Setter
@Entity
@Table(name = "tbl_student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "tbl_student_id_seq_generator")
    @SequenceGenerator(name = "tbl_student_id_seq_generator", sequenceName = "tbl_student_id_seq", allocationSize = 1)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "resits")
    private Integer resits;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "clazz_id")
    private Clazz clazz;

    @Override
    public String toString() {
        return MessageFormat.format("\nStudent[id {0}, name {1}, resits {2}]", id, name, resits);
    }
}
